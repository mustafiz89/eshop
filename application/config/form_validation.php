<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'login' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|min_length[6]|required'
        )
    ),
    'user' => array(
        array(
            'field' => 'full_name',
            'label' => 'Full Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]'
        ),
        array(
            'field' => 'conf_password',
            'label' => 'Confirm password',
            'rules' => 'trim|required|matches[password]'
        ),
        array(
            'field' => 'role',
            'label' => 'User role',
            'rules' => 'trim|required'
        ),
    ),
    'main_category' => array(
        array(
            'field' => 'main_category_name',
            'label' => 'Main category Name',
            'rules' => 'trim|required'
        )
    ),
    'category' => array(
        array(
            'field' => 'category_name',
            'label' => 'Category Name',
            'rules' => 'trim|required'
        )
    ),
    'sub_category' => array(
        array(
            'field' => 'sub_category_name',
            'label' => 'Sub category Name',
            'rules' => 'trim|required'
        )
    ),
    'product' => array(
        array(
            'field' => 'product_title',
            'label' => 'Product Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'product_price',
            'label' => 'Product Price',
            'rules' => 'trim|required|numeric'
        ),
        array(
            'field' => 'product_previous_price',
            'label' => 'Previous price',
            'rules' => 'trim|numeric'
        ),
        array(
            'field' => 'discount',
            'label' => 'Discount',
            'rules' => 'trim|numeric'
        )
//
    ),
    'customer' => array(
        array(
            'field' => 'f_name',
            'label' => 'First name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'l_name',
            'label' => 'Last name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        array(
            'field'=>'password',
            'label'=>'Password',
            'rules'=>'trim|required|min_length[6]'
        ),
        array(
            'field' => 'conf_password',
            'label' => 'Confirm password',
            'rules' => 'trim|required|matches[password]'
        )
//        array(
//            'field'=>'',
//            'label'=>'',
//            'rules'=>''
//        ),
    ),
    'customer_login' => array(
        array(
            'field' => 'email_2',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        array(
            'field' => 'password_2',
            'label' => 'Password',
            'rules' => 'trim|min_length[6]|required'
        )
    ),
    'update_customer' => array(
        array(
            'field' => 'f_name',
            'label' => 'First name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'l_name',
            'label' => 'Last name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email'
        ),
        array(
            'field'=>'old_password',
            'label'=>'Current Password',
            'rules'=>'trim|required|min_length[6]'
        ),
        array(
            'field'=>'password',
            'label'=>'Password',
            'rules'=>'trim|required|min_length[6]'
        ),
        array(
            'field' => 'conf_password',
            'label' => 'Confirm password',
            'rules' => 'trim|required|matches[password]'
        )
    ),
    'update_customer_address'=>array(
        array(
            'field'=>'mobile_phone',
            'label'=>'Mobile Phone No',
            'rules'=>'trim|required|numeric'
        ),
        array(
            'field'=>'home_phone',
            'label'=>'Home Phone no',
            'rules'=>'trim|numeric'
        ),
        array(
            'field'=>'address_1',
            'label'=>'Address',
            'rules'=>'trim|required'
        ),
        array(
            'field'=>'district_id',
            'label'=>'District name',
            'rules'=>'required'
        ),
    )
);