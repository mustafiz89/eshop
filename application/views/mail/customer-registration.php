<!DOCTYPE html">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>Seba24bd</title>
    <style type="text/css">
        /* Resets: see reset.css for details */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        table {border-spacing:0;}
        table td {border-collapse:collapse;}
        .yshortcuts a {border-bottom: none !important;}
        /* Constrain email width for small screens */
        @media screen and (max-width: 600px) {
            table[class="container"] {
                width: 95% !important;
            }
        }
        /* Give content more room on mobile */
        @media screen and (max-width: 480px) {
            td[class="container-padding"] {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }
        /* Styles for forcing columns to rows */
        @media only screen and (max-width : 600px) {
            /* force container columns to (horizontal) blocks */
            td[class="force-col"] {
                display: block;
                padding-right: 0 !important;
            }
            table[class="col-2"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;
                /* change left/right padding and margins to top/bottom ones */
                margin-bottom: 12px;
                padding-bottom: 12px;
                border-bottom: 1px solid #eee;
            }
            /* remove bottom border for last column/row */
            table[id="last-col-2"] {
                border-bottom: none !important;
                margin-bottom: 0;
            }
            /* align images right and shrink them a bit */
            img[class="col-2-img"] {
                float: right;
                margin-left: 6px;
                max-width: 130px;
            }
        }
    </style>
</head>
<body style="margin:0; padding:10px 0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- 100% wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
    <tr>
        <td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">
            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">
                <tr>
                    <td class="container-padding" bgcolor="#ffffff" style="background-color: #ffffff; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
                        <br>
                        <div style="font-weight: bold; font-size: 18px; line-height: 24px; color: #D03C0F;">
                            <h2 style="text-align: center">Eshop</h2>
                            <hr>

                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="container-padding" bgcolor="#ffffff" style="background-color: #ffffff; padding-left: 30px; padding-right: 30px; font-size: 13px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;" align="left">
                        <!--                        <div style="text-align: center;" class="center yellow">-->
                        <!--                            <img src="" alt="Seba24bd">-->
                        <!--                        </div>-->

                        <div>
                            <h2 style="text-align: center;" class="center yellow">Registration confirmation</h2>
                            <p class="user_details">
                                Dear <?=$name; ?>,<br>
                                Thanks for the registration in Eshop. Login to your email and password<br>
                                Your Email:  <?=$email?> <br>
                                Password: <?=$password?><br>
                                Now you can login to eshop and check your information like personal information, address information, order history and others.<br>
                                Thanks to stay with us.
                                <br><br>
                                If you have any question, please feel free to contact us at eshop@mustafiz.info.
                            </p>
                        </div>
                        <strong>Send by: eshop@mustafiz.info</strong><br>
                        <br>
                    </td>
                </tr>
            </table>
            <!--/600px container -->
        </td>
    </tr>
</table>
<!--/100% wrapper-->
</body>
</html>

