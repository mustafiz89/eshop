<div class="cart-block-content">
    <h5 class="cart-title"><span class="badge-warning">সর্বমোট পন্য <?=$this->cart->total_items()?> টি</span></h5>

    <div class="cart-block-list">
        <ul>
            <?php
            $data['cart_items']=$this->cart->contents();
            foreach($data['cart_items'] as $item)
            {
            ?>
            <li class="product-info">

                <div class="p-left">
                    <a href="#">
                        <img class="img-responsive" width="100px"
                             src="<?= base_url() ?>public/upload/product/thumbnail/<?=$item['option']['img']?>">
                    </a>

                </div>
                <div class="p-right">
                    <p class="p-name"><?=$item['name']?></p>

                    <p class="p-rice"><?=$item['subtotal']?> /=</p>

                    <p>Qty: <span class="badge-primary"><?=$item['qty']?></span></p>

                    <a class="btn btn-sm btn-primary remove-summary-cart-item" data-id="<?=$item['id']?>" title="Remove item from cart">x</a>
                </div>

            </li>
            <?php
            }
            ?>
        </ul>
    </div>
    <div class="total-cart">
        <span>মোট</span>
        <span class="toal-price pull-right"><?=$this->cart->total();?>/=</span>
    </div>
    <div class="cart-buttons">
        <a href="<?=base_url()?>home/order" class="btn btn-primary">Checkout <i class="fa fa-angle-double-right"></i></a>
    </div>
</div>
<script>
    //Remove Cart item By ajax
    $('.remove-summary-cart-item').on('click',function(e){
        e.preventDefault();
        var csrf='<?=$this->security->get_csrf_hash()?>';
        var id=$(this).attr('data-id');

        $.ajax({
            url:'<?=base_url()?>home/remove_cart_item',
            type:'post',
            data:{product_id:id,csrf:csrf},
            dataType:'json',
            success:function(response){
                $('.total').html(response.total+' ৳');
                $('.notify').html('<b>'+response.items+'</b>');
                $('.show_cart').html(response);

            },
            error:function(XHR,txtStatus,errorThrown){
                console.log('Error: '+errorThrown);
            }
        });
    });
</script>