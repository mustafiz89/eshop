<style>
    .modal-left-cart{
        border-right: 1px solid #e5e5e5;
    }
</style>
<!--Cart Modal-->
<div id="cart-modal" class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 style="color:#d77736">Cart Information</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6 modal-left-cart">
                            <h5 class="alert alert-info" style="font-weight: bold"><i class="fa fa-check-square-o"></i> Product successfully added to your shopping cart </h5>
                            <div class="col-sm-5">
                                <img id="product_img" src="" height="200" width="170">
                            </div>
                            <div class="col-sm-7">
                                <h4 id="product_title"></h4>
                                <hr>
                                <h5><b>Quantity: </b><span id="product_qty" class="badge"></span></h5>
                                <p><b>Total:</b> <span style="font-size: 16px" id="product_price"></span></p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4 id="product_total"></h4>
                            <hr>
                            <p style="font-size: 20px"><b>Total Price: </b><span id="total_price"></span> /=</p>


                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="continue-shipping" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Continue Shopping</button>
                <a href="<?=base_url()?>home/order"><button class="btn btn-success">Checkout <i class="fa fa-chevron-right"></i></button></a>
            </div>
        </div>
    </div>
</div>
<!--Cart Modal-->
<!-- Footer -->
<footer id="footer">
    <div class="container">
        <!-- introduce-box -->
        <div id="introduce-box" class="row">
            <div class="col-md-3">
                <div id="address-box">
                    <a href="#"><img src="<?=base_url()?>public/site/assets/images/logo2.png" alt="logo" /></a>
                    <div id="address-list">
                        <div class="tit-name">Address:</div>
                        <div class="tit-contain">খিলগাও, ঢাকা-১২১৪</div>
                        <div class="tit-name">মোবাইল:</div>
                        <div class="tit-contain">+88 01672770568</div>
                        <div class="tit-name">ইমেইল:</div>
                        <div class="tit-contain">support@eshop.com</div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div id="trademark-box" class="row">
                    <div class="col-sm-12">
                        <h2 class="introduce-title" style="text-align: center; font-size: 20px">পেমেন্ট মাধ্যম</h2>
                        <ul id="trademark-list">
                            <li>
                                <a href="#"><img src="<?=base_url()?>public/site/assets/data/cod.jpg" width="100px"  alt="img"/></a>
                            </li>

                            <li>
                                <a href="#"><img src="<?=base_url()?>public/site/assets/data/bkash.jpg" width="100px"  alt="img"/></a>
                            </li>
                            <li>
                                <a href="#"><img src="<?=base_url()?>public/site/assets/data/rocket.jpg" width="100px"  alt="img"/></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="contact-box">
                    <div class="introduce-title">নিউজলেটার</div>
                    <p style="font-size:11px;font-weight: bold; margin-bottom: 5px">আমাদের নতুন পণ্যের আপডেট পেতে এখনই সাবস্ক্রাইব করুন।</p>
                    <div class="input-group" id="mail-box">
                        <input type="text" placeholder="Your Email Address"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">সাবস্ক্রাইব</button>
                          </span>
                    </div><!-- /input-group -->
                    <div class="introduce-title">সামাজিক যোগাযোগ</div>
                    <div class="social-link">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#"><i class="fa fa-vk"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>

            </div>
        </div><!-- /#introduce-box -->

        <!-- #trademark-box -->
        <div id="footer-menu-box">
<!--            <div class="col-sm-12">-->
<!--                <ul class="footer-menu-list">-->
<!--                    <li><a href="#" >Company Info - Partnerships</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="col-sm-12">-->
<!--                <ul class="footer-menu-list">-->
<!--                    <li><a href="#" >Online Shopping</a></li>-->
<!--                    <li><a href="#" >Promotions</a></li>-->
<!--                    <li><a href="#" >My Orders</a></li>-->
<!--                    <li><a href="#" >Help</a></li>-->
<!--                    <li><a href="#" >Site Map</a></li>-->
<!--                    <li><a href="#" >Customer Service</a></li>-->
<!--                    <li><a href="#" >Support</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="col-sm-12">-->
<!--                <ul class="footer-menu-list">-->
<!--                    <li><a href="#" >Most Populars</a></li>-->
<!--                    <li><a href="#" >Best Sellers</a></li>-->
<!--                    <li><a href="#" >New Arrivals</a></li>-->
<!--                    <li><a href="#" >Special Products</a></li>-->
<!--                    <li><a href="#" >Manufacturers</a></li>-->
<!--                    <li><a href="#" >Our Stores</a></li>-->
<!--                    <li><a href="#" >Shipping</a></li>-->
<!--                    <li><a href="#" >Payments</a></li>-->
<!--                    <li><a href="#" >Warantee</a></li>-->
<!--                    <li><a href="#" >Refunds</a></li>-->
<!--                    <li><a href="#" >Checkout</a></li>-->
<!--                    <li><a href="#" >Discount</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="col-sm-12">-->
<!--                <ul class="footer-menu-list">-->
<!--                    <li><a href="#" >Terms & Conditions</a></li>-->
<!--                    <li><a href="#" >Policy</a></li>-->
<!--                    <li><a href="#" >Shipping</a></li>-->
<!--                    <li><a href="#" >Payments</a></li>-->
<!--                    <li><a href="#" >Returns</a></li>-->
<!--                    <li><a href="#" >Refunds</a></li>-->
<!--                    <li><a href="#" >Warrantee</a></li>-->
<!--                    <li><a href="#" >FAQ</a></li>-->
<!--                    <li><a href="#" >Contact</a></li>-->
<!--                </ul>-->
<!--            </div>-->
            <p class="text-center">কপিরাইট &#169; Eshop 2017 |
              Developed By <a target="_blank" href="http://dynamicsoftwareltd.com">Dynamic software Ltd</a></p>
        </div><!-- /#footer-menu-box -->
    </div>

</footer>
<script>
    $(function(){
//        Add product in cart
        $('.add-cart-product').on('click',function(){
           var p_id=$(this).attr('data-id');
           var qty=$(this).attr('data-qty');
           var csrf='<?=$this->security->get_csrf_hash()?>';
            $.ajax({
                url:'<?=base_url()?>home/add_product_to_cart',
                type:'post',
                data:{product_id:p_id,csrf:csrf,qty:qty},
                dataType:'json',
                success:function(response){
                    $('#product_title').html(response.cart_item.name);
                    $('#product_qty').html(response.cart_item.qty);
                    $('#product_price').html(response.cart_item.subtotal+'/=');
                    $('#product_total').html('There are&nbsp'+ response.items+' Product in your cart');
                    $('#total_price').html(response.total);
                    $('.notify').html('<b>'+response.items+'</b>');
                    $('#product_img').attr('src','<?=base_url()?>public/upload/product/thumbnail/'+response.cart_item.option.img);
                    $('#cart-modal').modal('show');
                },
                error:function(XHR,txtStatus,errorThrown){
                    console.log('Error: '+errorThrown);
                }
            });

        });

//        Close modal
        $('#continue-shipping').click(function(){
            $('#cart-modal').modal('hide');
        });
    });
    $('.cart-hover').hover(function(){
        $.ajax({
            url:'<?=base_url()?>home/show_cart',
            type:'GET',
            success:function(response){
                $('.show_cart').html(response);
            },
            error:function(XHR,txtStatus,errorThrown){
                console.log('Error: '+errorThrown);
            }
        });
    });

//    $('.show_cart_div').on('click',function(){
//        $.ajax({
//           url:'<?//=base_url()?>//home/show_cart',
//           type:'GET',
//            success:function(response){
//                $('.show_cart').html(response);
//            },
//            error:function(XHR,txtStatus,errorThrown){
//                console.log('Error: '+errorThrown);
//            }
//        });
//    });

</script>
<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/owl.carousel/owl.carousel.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/jquery.elevatezoom.js"></script>

<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>public/site/assets/lib/fancyBox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/site/assets/js/jquery.actual.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/site/assets/js/theme-script.js"></script>

</body>
</html>