<!--Home Slider-->
<?php $this->load->view('shop/layout/home-slider') ?>
<!--Home Slider-->

<div class="content-page">
    <div class="container">
        <?php
        foreach ($all_widget_category as $w) {
            ?>
            <!-- featured category fashion -->
            <div class="category-featured fashion">
                <div class="product-featured clearfix">
                    <div class="row">
                        <div class="col-sm-2 sub-category-wapper">
                            <div style="background:<?= $w->widget_color ?>" class="navbar-brand"><a
                                    href="#"><?= $w->widget_title ?></a></div>
                            <ul class="sub-category-list">
                                <?php
                                $category = new Category_model();
                                $data['all_sub_category'] = $category->selectAllActiveSubCategoryByCategoryId($w->category_id);
                                foreach ($data['all_sub_category'] as $s) {
                                    ?>
                                    <li><a href="<?=base_url()?>home/product_by_sub_category/<?=$s->sub_category_id?>"><i class="fa fa-link"></i> <?= $s->sub_category_name ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-sm-10 col-right-tab">
                            <div class="product-featured-tab-content">
                                <div class="tab-container" style="border-top:2px solid <?= $w->widget_color ?>">
                                    <div class="tab-panel active" id="tab-4">
                                        <div class="box-left">
                                            <div class="banner-img">
                                                <div class="box-product-adv">
                                                    <ul class="owl-carousel nav-center">
                                                        <?php
                                                        $data['slider_image'] = $category->selectAllCategoryImageById($w->category_id);
                                                        foreach ($data['slider_image'] as $i) {
                                                            ?>
                                                            <li><a><img src="<?= base_url() . $i->widget_image ?>" alt="No Slider Image"></a></li>
                                                        <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-right">
                                            <ul class="product-list row">
                                                <!--  Start product Loop-->
                                                <?php
                                                $product = new Product_model();
                                                $data['product'] = $product->selectProductByCategoryId($w->category_id);
                                                foreach ($data['product'] as $p) {
                                                    ?>
                                                    <li class="col-sm-4">
                                                        <div class="right-block">
                                                            <h5 class="product-name">
                                                                <a target="_blank" href="<?=base_url()?>home/product_details/<?=$p->product_id?>">
                                                                    <?= $p->product_title ?></a>&nbsp <br><span style="color:<?= $w->widget_color ?>">৳ <?= $p->product_price ?> </span>

                                                            </h5>

                                                        </div>
                                                        <div class="left-block">
                                                            <a href="<?=base_url()?>home/product_details/<?=$p->product_id?>"><img  alt="product"
                                                                             src="<?= base_url() ?>public/upload/product/thumbnail/<?=$p->product_default_image ?>"></a>

                                                            <div class="quick-view">
                                                                <a href="#" title="Add to my wishlist" class="heart" onMouseOver="this.style.background='<?=$w->widget_color?>'" onMouseOut="this.style.background=''"></a>
                                                            </div>
                                                            <div class="add-to-cart" style="background-color: <?= $w->widget_color ?>">
                                                                <a class="add-cart-product" title="Add to Cart" data-id="<?=$p->product_id?>" data-qty="1" >কার্টে যুক্ত করুন</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end featured category fashion -->
        <?php
        }
        ?>
    </div>
</div>
<div id="content-wrap">
    <div class="container">
        <!-- Baner bottom -->
        <div class="row banner-bottom">
        </div>
    </div>
    <!-- /.container -->
</div>
<script>

    $(document).ready(function () {

        $(".owl-carousel").owlCarousel({
            autoPlay: 3000,
            navigation: true, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 600,
            singleItem: true

        });

        $("#owl-carousel").owlCarousel({
            autoPlay: 3000 //Set AutoPlay to 3 seconds
        });

    });

</script>