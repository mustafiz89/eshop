<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Shopping cart</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2">Shopping Cart Summary</span>
            <hr>
        </h2>

        <div class="page-content page-order" id="cart-summary">
            <ul class="step">
                <li class="<?php if(isset($summary))echo $summary;?>"><span>01. Summary</span></li>
                <li class="<?php if(isset($signin))echo $signin;?>"><span>02. Sign in</span></li>
                <li class="<?php if(isset($address))echo $address;?>"><span>03. Address</span></li>
<!--                <li><span>04. Shipping</span></li>-->
                <li class="<?php if(isset($payment))echo $payment;?>"><span>04. Payment</span></li>
            </ul>

        <!-- ../page heading-->
        <?php if(isset($cart_content)){
            echo $cart_content;

        } else{
            echo '<br><br><h3 class="alert alert-warning"><i class="fa fa-info-circle"></i> You have no product in your cart</h3>';
        }?>
        </div>
    </div>
</div>
<!-- ./page wapper-->