<style>
    .req{
        color:#ff1e08;
    }
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Your Account</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Your Profile Information</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Profile Information</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <p>Please be sure to update your personal information if it has changed.</p>
                            <p style="color:#ff1e08">* Required field</p><br>
                            <div class="col-sm-4">
                                <form action="<?= base_url('home/update_personal_info') ?>" method="post">
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                           value="<?= $this->security->get_csrf_hash() ?>"/>

                                    <div class="form-group ">
                                        <label>First Name</label><span class="req"> *</span>
                                        <input type="text" class="form-control" placeholder="First Name" name="f_name"
                                              value="<?=$customer->f_name?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Last Name</label><span class="req"> *</span>
                                        <input type="text" class="form-control" placeholder="Last Name" name="l_name"
                                              value="<?=$customer->l_name?>" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label><span class="req"> *</span>
                                        <input id="email" type="email" class="form-control" placeholder="Email"
                                               name="email" value="<?=$customer->email?>" required>
                                    <span id="email_alert">

                                    </span>
                                        <span style="color:red"></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Gender</label><span class="req"></span>
                                       <select class="form-control" name="gender" required>
                                           <?php if($customer->gender==="Female"){?>
                                               <option value="Male">Male</option>
                                               <option value="Female" <?php echo 'selected'?>>Female</option>
                                           <?php } else {?>
                                               <option value="Male" <?php echo 'selected'?>>Male</option>
                                               <option value="Female">Female</option>
                                           <?php } ?>
                                       </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Current Password</label><span class="req"> *</span>
                                        <input type="password" class="form-control"
                                               placeholder="Current password" name="old_password" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input id="pass" type="password" class="form-control"
                                               placeholder="Minimum 6 character" name="password" required value="">
                                        <span style="color:red"></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input id="conf_pass" type="password" class="form-control"
                                               placeholder="Confirm Password" name="conf_password" required>
                                        <span class="check"></span>
                                        <span style="color:red"></span>
                                    </div>

                                    <button type="submit" class="btn btn-primary"><i class="fa fa-user-plus"></i> Update
                                    </button>
                                    <a href="<?=base_url('home/customer_profile
                                    ')?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to your account</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ./page wapper-->
<script type="text/javascript" src="<?=base_url()?>public/site/assets/js/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>public/site/assets/js/datepicker.js"></script>
<script>
    $(function() {
        $('input[name="dob"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });


        $('#conf_pass').on('keyup', function () {
            var conf_pass = $(this).val();
            var pass = $('#pass').val();
            if (conf_pass == pass) {
                $('.check').html('<p class="label label-success"> Password Matched</p>');
            }
            else {
                $('.check').html('<p class="label label-danger"> Password not matched</p>')
            }
        });
        $('#email').on('change', function () {
            var email = $(this).val();
            var csrf = '<?=$this->security->get_csrf_hash()?>';
            $.ajax({
                url: '<?=base_url()?>home/check_email',
                type: 'post',
                data: {email: email, csrf: csrf},
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        $('#email_alert').html('<p class="label label-success">' + response.message + '</p>');
                    }
                    else {
                        $('#email_alert').html('<p class="label label-danger">' + response.message + '</p>');
                    }
                },
                error: function (XHR, txtStatus, errorThrown) {
                    console.log('Error: ' + errorThrown);
                }
            });
        });
    });
</script>
