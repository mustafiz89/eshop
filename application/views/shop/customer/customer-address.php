<style>
    .req{
        color:#ff1e08;
    }
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Your Account</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Your Address</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Address Information</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <p>To add/update your address, please fill out the form below. </p>
                            <p style="color:#ff1e08">* Required field</p><br>
                            <div class="col-sm-4">
                                <form action="<?= base_url('home/update_customer_address') ?>" method="post">
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                           value="<?= $this->security->get_csrf_hash() ?>"/>

                                    <div class="form-group ">
                                        <label>First Name</label><span class="req"> *</span>
                                        <input type="text" class="form-control" placeholder="First Name"
                                               value="<?=$customer->f_name?>" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label>Last Name</label><span class="req"> *</span>
                                        <input type="text" class="form-control" placeholder="Last Name"
                                               value="<?=$customer->l_name?>" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label>Mobile Phone</label><span class="req"> *</span>
                                        <input type="text" class="form-control" placeholder="Mobile Phone" name="mobile_phone"
                                               value="<?=$customer->mobile_phone?>" required>
                                        <span class="label label-danger">** You must register at least one mobile phone number.</span>
                                    </div>
                                    <div class="form-group">
                                        <label>Home Phone</label>
                                        <input type="text" class="form-control" placeholder="Home Phone" name="home_phone"
                                               value="<?=$customer->home_phone?>">
                                    </div>


                                    <div class="form-group">
                                        <label>Address </label><span class="req"> *</span>
                                        <textarea class="form-control" name="address_1" rows="5"><?=$customer->address_1?></textarea>
                                   </div>
                                    <div class="form-group">
                                        <label>Address (Line 2)</label>
                                        <textarea class="form-control" name="address_2" rows="5"><?=$customer->address_2?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>District</label><span class="req"> *</span>
                                        <select class="form-control" name="district_id" required>
                                            <option value="">--SELECT--</option>
                                            <?php foreach($district as $d){?>
                                                <option value="<?=$d->district_id?>" <?php if($customer->district_id==$d->district_id) echo 'selected'?>>
                                                <?=$d->district_name?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Additional information</label>
                                        <textarea class="form-control" name="additional_information" rows="5"><?=$customer->additional_information?></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update Address
                                    </button>
                                    <a href="<?=base_url('home/customer_profile
                                    ')?>" class="btn btn-default"><i class="fa fa-reply"></i> Back to your account</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ./page wapper-->