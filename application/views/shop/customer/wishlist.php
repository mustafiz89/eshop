<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" title="Return to Home">My account</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My wishlist</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My wishlist</span>
                </h2>
                <!-- ../page heading-->
                <div class="box-border box-wishlist">
                    <h2>New wishlist</h2>
                    <label for="wishlist-name">Name</label>
                    <input type="text" class="form-control input">
                    <button class="button">Save</button>
                </div>
                <table class="table table-bordered table-wishlist">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Viewed</th>
                        <th>Created</th>
                        <th>Derect link</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>My wishlist</td>
                        <td>7</td>
                        <td>0</td>
                        <td>2015-06-18</td>
                        <td>View</td>
                        <td class="text-center"><a href="#"><i class="fa fa-close"></i></a></td>
                    </tr>
                    </tbody>
                </table>
                <ul class="row list-wishlist">
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist1.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist2.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist3.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist4.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <button class="button button-sm">Save</button>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist5.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist6.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist7.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                    <li class="col-sm-3">
                        <div class="product-img">
                            <a href="#"><img src="<?=base_url()?>public/site/assets/data/wishlist8.jpg" alt="Product"></a>
                        </div>
                        <h5 class="product-name">
                            <a href="#">Adipiscing aliquet sed in lacus, Liqui-gels 24</a>
                        </h5>
                        <div class="qty">
                            <label>Quantity</label>
                            <input type="text" class="form-control input input-sm">
                        </div>
                        <div class="priority">
                            <label>Priority</label>
                            <select class="form-control input iput-sm">
                                <option>Medium</option>
                            </select>
                        </div>
                        <div class="button-action">
                            <button class="button button-sm">Save</button>
                            <a href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
