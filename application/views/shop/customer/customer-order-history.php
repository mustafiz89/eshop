<style>
    .req {
        color: #ff1e08;
    }
    th td{
        text-align: center;
    }
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">My Account</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Order</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Order history and detials</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Order History and Payments</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <table id="order-list" class="table table-bordered table-hovered">
                                <thead>
                                <tr>
                                    <th style="width: 8%">SN</th>
                                    <th>Order reference</th>
                                    <th style="width: 10%">Invoice</th>
                                    <th style="width: 14%">Date</th>
                                    <th style="width: 10%">Total price</th>
                                    <th style="width: 13%">Payment</th>
                                    <th style="width: 10%">Confirmation</th>
                                    <th style="width: 10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0; foreach($order as $o){ $i+=1;?>
                                <tr>
                                    <td><?=$i?></td>
                                    <td><?=$o->order_id;?></td>
                                    <td><?=$o->invoice_id?></td>
                                    <td>
                                        <?php
                                            $date=strtotime($o->created_at);
                                            echo date('d M Y',$date);
                                        ?>
                                    </td>
                                    <td><?=$o->total?>/=</td>
                                    <td><span class="label label-info"><?=$o->payment_method?></span></td>
                                    <td>
                                        <?php if($o->confirmed_status==0){
                                            echo '<label class="label label-danger"> Pending </label>';
                                        }
                                        else{
                                            echo '<label class="label label-success">
                                             Success </label>';
                                        }?>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-warning btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Action
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a class="details" data-id="<?=$o->order_table_id?>"><i class="fa fa-search"></i> View Details</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Order Details</h4>
            </div>
            <div id="show_table" class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('#order-list').dataTable({
        "sPaginationType": "full_numbers"
    });

    $('.details').on('click',function(){
       var id=$(this).attr('data-id');
        var csrf='<?=$this->security->get_csrf_hash()?>';
        $.ajax({
            url:'<?=base_url()?>home/view_details_order',
            type:'post',
            data:{id:id,csrf:csrf},
//            dataType:'json',
            success:function(response){
                $('#show_table').html(response);
                $('#modal').modal('show');
            },
            error:function(XHR,txtStatus,errorThrown){
                console.log('Error: '+errorThrown);
            }
        });
    });
</script>
<!-- ./page wapper-->