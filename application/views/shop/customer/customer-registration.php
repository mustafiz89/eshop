<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Authentication</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Authentication</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Create an account</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <div class="col-sm-8">
                                <form action="<?= base_url('home/save_customer') ?>" method="post">
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                           value="<?= $this->security->get_csrf_hash() ?>"/>

                                    <div class="form-group <?php if ($this->session->flashdata('f_name')) {
                                        echo "has-error";
                                    } ?>">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" placeholder="First Name" name="f_name"
                                               required>
                                        <span style="color:red"><?= $this->session->flashdata('f_name') ?></span>

                                    </div>

                                    <div class="form-group <?php if ($this->session->flashdata('l_name')) {
                                        echo "has-error";
                                    } ?>">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" placeholder="Last Name" name="l_name"
                                               required>
                                        <span style="color:red"><?= $this->session->flashdata('l_name') ?></span>

                                    </div>

                                    <div class="form-group <?php if ($this->session->flashdata('l_name')) {
                                        echo "has-error";
                                    } ?>">
                                        <label>Email</label>
                                        <input id="email" type="email" class="form-control" placeholder="Email"
                                               name="email" required>
                                    <span id="email_alert">

                                    </span>
                                        <span style="color:red"><?= $this->session->flashdata('email') ?></span>
                                    </div>

                                    <div class="form-group <?php if ($this->session->flashdata('password')) {
                                        echo "has-error";
                                    } ?>">
                                        <label>Password</label>
                                        <input id="pass" type="password" class="form-control"
                                               placeholder="Minimum 6 character" name="password" required value="">
                                        <span style="color:red"><?= $this->session->flashdata('password') ?></span>
                                    </div>

                                    <div class="form-group <?php if ($this->session->flashdata('password')) {
                                        echo "has-error";
                                    } ?>">
                                        <label>Confirm Password</label>
                                        <input id="conf_pass" type="password" class="form-control"
                                               placeholder="Confirm Password" name="conf_password" required>
                                        <span class="check"></span>
                                        <span style="color:red"><?= $this->session->flashdata('conf_password') ?></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Gender</label> &nbsp&nbsp&nbsp
                                        <input type="radio" value="Male" name="gender" checked> Male &nbsp&nbsp&nbsp
                                        <input type="radio" value="Female" name="gender"> Female

                                    </div>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create
                                        Account
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Already registered?</h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            $er = $this->session->userdata('login_error');
                            if ($er)
                            {
                            ?>
                            <h5 id="error" class="alert alert-danger">
                                <?php
                                echo '<i class="fa fa-close"></i> ' . $er . '</h5>';
                                $this->session->unset_userdata('login_error');
                                }
                                ?>
                                <div class="col-sm-8">
                                    <form action="<?= base_url('home/login_customer') ?>" method="post">
                                        <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                               value="<?= $this->security->get_csrf_hash() ?>"/>

                                        <div class="form-group <?php if ($this->session->flashdata('email_2')) {
                                            echo "has-error";
                                        } ?>">
                                            <label>Email Address</label>
                                            <input type="email" class="form-control" name="email_2" placeholder="Email"
                                                   required>
                                            <span style="color:red"><?= $this->session->flashdata('email_2') ?></span>
                                        </div>
                                        <div class="form-group <?php if ($this->session->flashdata('password_2')) {
                                            echo "has-error";
                                        } ?>">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password"
                                                   name="password_2" required>
                                            <span style="color:red"><?= $this->session->flashdata('password_2') ?></span>
                                        </div>
                                        <div class="form-group">
                                            <a href="">Forget your password</a>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Sign
                                            In
                                        </button>
                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ./page wapper-->

<script>
    $('#conf_pass').on('keyup', function () {
        var conf_pass = $(this).val();
        var pass = $('#pass').val();
        if (conf_pass == pass) {
            $('.check').html('<p class="label label-success"> Password Matched</p>');
        }
        else {
            $('.check').html('<p class="label label-danger"> Password not matched</p>')
        }
    });

    $('#email').on('change', function () {
        var email = $(this).val();
        var csrf = '<?=$this->security->get_csrf_hash()?>';
        $.ajax({
            url: '<?=base_url()?>home/check_email',
            type: 'post',
            data: {email: email, csrf: csrf},
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    $('#email_alert').html('<p class="label label-success">' + response.message + '</p>');
                }
                else {
                    $('#email_alert').html('<p class="label label-danger">' + response.message + '</p>');
                }
            },
            error: function (XHR, txtStatus, errorThrown) {
                console.log('Error: ' + errorThrown);
            }
        });
    });
</script>