<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">My Profile</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Account Information</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <div class="col-md-6">
                                <div class="list-group">
                                    <h3></span><a href="<?=base_url()?>home/show_personal_info" class="list-group-item list-group-item-success"><i
                                                class="glyphicon glyphicon-info-sign" aria-hidden="true"></i> My Personal
                                            Information</a></h3>
                                    <br>
                                    <h3></span><a href="<?=base_url()?>home/show_customer_address" class="list-group-item list-group-item-info"><i
                                                class=" glyphicon glyphicon-list-alt" aria-hidden="true"></i> My Address</a></h3>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="list-group">
                                    <h3></span><a href="" class="list-group-item list-group-item-warning"><i
                                                class="glyphicon glyphicon-heart" aria-hidden="true"></i> My Wishlist</a></h3>
                                    <br>
                                    <h3></span><a href="<?=base_url()?>home/customer_order_history" class="list-group-item list-group-item-danger"><i
                                                class="glyphicon glyphicon-list" aria-hidden="true"></i> Order history and details</a></h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- ./page wapper-->
