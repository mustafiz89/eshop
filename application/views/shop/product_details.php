
<div class="columns-container">

    <div class="container" id="columns">
        <!-- breadcrumb -->
        <hr>
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">মূলপাতা</a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#"><?=$product->main_category_name?></a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" ><?=$product->category_name?></a>
            <span class="navigation-pipe">&nbsp;</span>
            <a href="#" ><?=$product->sub_category_name?></a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><?=$product->product_title?></span>
        </div>
        <br>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- Product -->
                <div id="product">
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-sm-5">
                            <!-- product-imge-->
                            <div class="product-image">
                                <div class="product-full" id="gallery_01">
                                    <img id="product-zoom" src="<?=base_url()?>public/upload/product/thumbnail/<?=$product->product_default_image?>"
                                         data-zoom-image="<?=base_url()?>public/upload/product/<?=$product->product_default_image?>"/>
                                </div>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-sm-7">
                            <h1 class="product-name"><?=$product->product_title?></h1>
                            <div class="product-price-group">
                                <span class="price">৳ <?=$product->product_price?></span>
                                <?php if($product->product_previous_price){?>
                                <span class="old-price">৳ <?=$product->product_previous_price?></span>
                                <?php }?>
                                <?php if($product->discount){?>
                                    <span class="discount">৳ <?=$product->discount?>%</span>
                                <?php }?>
                            </div>
                            <div class="info-orther">
<!--                                <p>Item Code: #453217907</p>-->

                                <p class="availability">উপস্থিতিঃ 
                                    <?php
                                    if($product->stock_status==1){
                                        echo '<span>স্টকে আছে</span>';
                                    }
                                    else{
                                        echo '<span class="in-stock" style="color:#E13300">স্টক শেষ</span>';
                                    }
                                    ?>

                                </p>

                                <p><b>Condition: <?php
                                    if($product->product_condition==1){
                                        echo '<span>New</span>';
                                    }
                                    else if($product->product_condition==2){
                                        echo '<span>Brand New</span>';
                                    }
                                    else if($product->product_condition==3){
                                        echo '<span>Used</span>';
                                    }
                                    else{
                                        echo '<span>Old</span>';
                                    }
                                    ?>
                                    </b>
                                </p>
                            </div>

                            <div class="form-option">
                                <p class="form-option-title">Available Options:</p>

                                <div class="attributes">
                                    <div class="attribute-label">Qty:</div>
                                    <div class="attribute-list product-qty">
                                        <div>
                                            <input id="qty" type="number" value="1" min="1"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="attributes">
                                    <div class="attribute-label"><b>Size:</b></div>
                                    <div class="attribute-list">

                                    </div>

                                </div>
                            </div>
                            <div class="form-action">
                                <div class="button-group">
                                   <button class="btn-add-cart" id="add-cart-product" data-id="<?=$product->product_id?>" >কার্টে যুক্ত করুন</button>
                                </div>
                                <div class="button-group">
                                    <a class="btn btn-success" href="<?=base_url()?>home/order">অর্ডার করুন <i class="fa fa-caret-right"></i></a>
                                </div>

<!--                                <div class="button-group">-->
<!--                                    <a class="wishlist" href="#"><i class="fa fa-heart-o"></i>-->
<!--                                        <br>Wishlist</a>-->
<!--                                </div>-->
                            </div>
                            <div class="form-share">
                                <div class="sendtofriend-print">
                                    <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab product -->
                    <div class="product-tab">
                        <ul class="nav-tab">
                            <li class="active">
                                <a aria-expanded="false" data-toggle="tab" href="#product-detail">Features</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#technical">Technical Specification</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#support">Supports</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#manual">Manual</a>
                            </li>

                        </ul>
                        <div class="tab-container">
                            <div id="product-detail" class="tab-panel active">
                                <?=$product->product_details?>
                            </div>
                            <div id="technical" class="tab-panel">
                                <?=$product->technical?>
                            </div>
                            <div id="support" class="tab-panel">
                                <?=$product->support?>
                            </div>
                            <div id="Manual" class="tab-panel">
                                <?=$product->manual?>
                            </div>
                        </div>
                    </div>
                    <!-- ./tab product -->
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
        <br>
        <div class="col-xs-12 col-sm-12">
        <div id="view-product-list" class="view-product-list">
            <h2 class="page-heading">
                <span class="page-heading-title">Related Product</span>
            </h2>
        <div class="row">
            <ul class="row product-list grid">
                <?php
                foreach($related_product as $p){
                    ?>
                    <li class="col-sx-12 col-sm-3">
                        <div class="product-container">
                            <div class="left-block">
                                <a href="">
                                    <img class="img-responsive" alt="product" src="<?=base_url()?>public/upload/product/thumbnail/<?=$p->product_default_image?>" />
                                </a>
                                <div class="quick-view">
                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                    <a title="View Details" class="search" href="#"></a>
                                </div>
                                <div class="add-to-cart">
                                    <a class="add-cart-product" title="Add to Cart" data-id="<?=$p->product_id?>" data-qty="1" >কার্টে যুক্ত করুন</a>
                                </div>
                            </div>
                            <div class="right-block">
                                <h5 class="product-name">
                                    <a href="<?=base_url()?>home/product_details/<?=$p->product_id?>">
                                        <?=$p->product_title?></a></h5>
                                <div class="content_price">
                                    <span class="price product-price">৳ <?=$p->product_price?></span>
                                    <?php if($p->product_previous_price){
                                        echo '<span class="price product-price old-price" style="color:#E13300; font-weight: bold">৳ '.$p->product_previous_price.'</span>';
                                    }?>
                                </div>
                                <div class="info-orther">
                                    <!--                                        <p>Item Code: #453217907</p>-->
                                    <?php if($p->discount>0){
                                        echo '<span class="discount" style="font-weight: bold">('.$p->discount.'% Discount)</span>';
                                    }?>
                                    <p class="availability">উপস্থিতি:
                                        <?php
                                        if($p->stock_status==1){
                                            echo '<span>স্টকে আছে</span>';
                                        }
                                        else{
                                            echo '<span style="color:#E13300">স্টক শেষ</span>';
                                        }
                                        ?>

                                    </p>
                                    <div class="product-desc">
                                        <?=$p->product_details?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php }?>
            </ul>
        </div>
            </div>
            </div>
    </div>
</div>
<script>
    $(function(){
//        Add product in cart
        $('#add-cart-product').on('click',function(){
            var p_id=$(this).attr('data-id');
            var qty=$('#qty').val();
            var csrf='<?=$this->security->get_csrf_hash()?>';
            $.ajax({
                url:'<?=base_url()?>home/add_product_to_cart',
                type:'post',
                data:{product_id:p_id,csrf:csrf,qty:qty},
                dataType:'json',
                success:function(response){
                    $('#product_title').html(response.cart_item.name);
                    $('#product_qty').html(response.cart_item.qty);
                    $('#product_price').html(response.cart_item.subtotal+'/=');
                    $('#product_total').html('There are&nbsp'+ response.items+' Product in your cart');
                    $('#total_price').html(response.total);
                    $('.notify').html('<b>'+response.items+'</b>');
                    $('#product_img').attr('src','<?=base_url()?>public/upload/product/thumbnail/'+response.cart_item.option.img);
                    $('#cart-modal').modal('show');
                },
                error:function(XHR,txtStatus,errorThrown){
                    console.log('Error: '+errorThrown);
                }
            });

        });

//        Close modal
        $('#continue-shipping').click(function(){
            $('#cart-modal').modal('hide');
        });
    });
</script>