
<div class="heading-counter warning">You have <span><?=$this->cart->total_items()?></span> product in your shopping cart

</div>
<div class="order-detail-content">
    <table class="table table-bordered table-responsive cart_summary">
        <thead>
        <tr>
            <th class="cart_product">Product</th>
            <th>Description</th>
            <th>Avail.</th>
            <th>Unit price</th>
            <th>Qty</th>
            <th>Total</th>
            <th  class="action"><i class="fa fa-trash-o"></i></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($this->cart->contents() as $item){
            ?>
            <tr>
                <td class="cart_product">
                    <a href="#"><img src="<?=base_url()?>public/upload/product/thumbnail/<?=$item['option']['img']?>" alt="Product" width="100"></a>
                </td>
                <td class="cart_description">
                    <p class="product-name"><a href="#"><?=$item['name']?></a></p>
                    <small class="cart_ref"></small><br>
                    <small><a href="#">Color : Beige</a></small><br>
                    <small><a href="#">Size : S</a></small>
                </td>
                <td class="cart_avail">
                    <?php
                    if($item['option']['stock']==1){
                        echo '<span class="label label-success">In Stock</span>';
                    }
                    else{
                        echo '<span class="label label-danger">Out of Stock</span>';
                    }
                    ?>
                </td>
                <td class="price"><span><?=$item['price']?> ৳</span></td>
                <td class="qty">
                    <input  class="input-sm item-qty" type="number" value="<?=$item['qty']?>" min="1" data-id="<?=$item['id']?>">
                </td>
                <td class="price">
                    <span id="subtotal-<?=$item['id']?>"> <?=$item['subtotal']?> ৳</span>
                </td>
                <td class="action">
                    <a class="remove-cart-item" href="" data-id="<?=$item['id']?>">Delete item</a>
                </td>
            </tr>
        <?php }?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2" rowspan="2"></td>
            <td colspan="3">Total products (tax incl.)</td>
            <td class="total" colspan="2"><?=$this->cart->total();?> ৳</td>
        </tr>
        <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td colspan="2"><strong class="total"><?=$this->cart->total();?> ৳ </strong></td>
        </tr>
        </tfoot>
    </table>
    <div class="cart_navigation">
        <a class="btn btn-warning" href="#"><i class="fa fa-angle-double-left"></i> Continue shopping</a>
        <a class="btn btn-primary pull-right" href="<?=base_url()?>home/order?step=1">Proceed to checkout <i class="fa fa-angle-double-right"></i></a>
    </div>
</div>
<script>

    //    Update Cart Item by Ajax

    $('.item-qty').on('change',function(){
        var qty=$(this).val();
        var id=$(this).attr('data-id');
        var csrf='<?=$this->security->get_csrf_hash()?>';
        $.ajax({
            url:'<?=base_url()?>home/update_quantity',
            type:'post',
            data:{product_id:id,csrf:csrf,qty:qty},
            dataType:'json',
            success:function(response){
                $('#subtotal-'+response.product.id).html(response.product.subtotal+' ৳');
                $('.total').html(response.total+' ৳');
                $('.notify').html('<b>'+response.items+'</b>');
            },
            error:function(XHR,txtStatus,errorThrown){
                console.log('Error: '+errorThrown);
            }
        });
    });
    $('.remove-cart-item').on('click',function(e){
        e.preventDefault();
        var csrf='<?=$this->security->get_csrf_hash()?>';
        var id=$(this).attr('data-id');
        $.ajax({
            url:'<?=base_url()?>home/remove_cart_item',
            type:'post',
            data:{product_id:id,csrf:csrf},
            dataType:'json',
            success:function(response){
                $('.total').html(response.total+' ৳');
                $('.notify').html('<b>'+response.items+'</b>');
                $('#cart-summary').html(response.cart_summary);

            },
            error:function(XHR,txtStatus,errorThrown){
                console.log('Error: '+errorThrown);
            }
        });
    });
</script>