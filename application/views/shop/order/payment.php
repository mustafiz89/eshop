<style>
    .req{
        color:#ff1e08;
    }
</style>
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Order</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Payment </span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Payment Information</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Delivery / Billing Address</h2>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-7">
                                <h4 style="padding-bottom: 2px; border-bottom: 1px solid #aaa">
                                    <?=$customer->f_name.' '.$customer->l_name;?>
                                </h4>
                                <h5 style="font-size: 14px; padding-top: 2px;"><?=$customer->address_1?></h5>
                                <h5><?=$customer->district_name?>.</h5>
                                <h5>Mobile: <?=$customer->mobile_phone?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Payment Method</h2>
                        </div>
                        <div class="panel-body">
                            <div>
                                <?php $this->load->view('shop/layout/notification') ?>
                            </div>
                            <p>Add a Payment method to confirm order finally </p>
                            <div class="col-sm-12">
                                <form id="payment_form" action="<?= base_url('home/confirm_order_for_delivery') ?>" method="post">
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                           value="<?= $this->security->get_csrf_hash() ?>"/>
                                    <div class="radio">
                                        <label>
                                            <input class="radio_button" type="radio" name="payment_method" value="cash_on_delivery">
                                            Cash on Delivery
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input class="radio_button" type="radio" name="payment_method" value="Bkash">
                                        Bkash
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input class="radio_button" type="radio" name="payment_method" value="Rocket">
                                            Rocket(DBBL)
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label>Additional message</label>
                                        <textarea class="form-control" name="additional_message" rows="5"></textarea>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input id="term" type="checkbox" value="">
                                            I agree to the terms and finally ready for order confirmation.
                                        </label>
                                    </div>


                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Order Confirmation
                                    </button>
                                    <a href="<?=base_url('home/order')?>" class="btn btn-info">
                                        <i class="fa fa-check"></i> Order summary</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
<script>
    $('#payment_form').submit(function(){
       var term=$("#term").is(':checked') ? 1 : 0;
        if($('input[name=payment_method]:checked').length<=0)
        {
            alert("Please select a payment method");
            return false;
        }
        else if(term==1){
            return true;
        }
        else if(term==0){
            alert('Please select order confirmation');
            return false;
        }
    });
</script>