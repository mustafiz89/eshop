<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Order</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Authentication</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">Authentication</span>
        </h2>

        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h2>Login to your email and password</h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            $er = $this->session->userdata('login_error');
                            if ($er)
                            {
                            ?>
                            <h5 id="error" class="alert alert-danger">
                                <?php
                                echo '<i class="fa fa-close"></i> ' . $er . '</h5>';
                                $this->session->unset_userdata('login_error');
                                }
                                ?>
                                <div class="col-sm-4">
                                    <form action="<?=base_url('home/login_customer_from_order') ?>" method="post">
                                        <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                                               value="<?= $this->security->get_csrf_hash() ?>"/>

                                        <div class="form-group <?php if ($this->session->flashdata('email_2')) {
                                            echo "has-error";
                                        } ?>">
                                            <label>Email Address</label>
                                            <input type="email" class="form-control" name="email_2" placeholder="Email"
                                                   required value="">
                                            <span style="color:red"><?= $this->session->flashdata('email_2') ?></span>
                                        </div>
                                        <div class="form-group <?php if ($this->session->flashdata('password_2')) {
                                            echo "has-error";
                                        } ?>">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Password"
                                                   name="password_2" required>
                                            <span style="color:red"><?= $this->session->flashdata('password_2') ?></span>
                                        </div>
                                        <div class="form-group">
                                            <a href="">Forget your password</a>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in">
                                            </i> Sign In </button>


                                    </form>
                                </div>
                        </div>
                    </div>
                    <a href="<?=base_url()?>home/order" class="btn btn-info pull-right"><i class="fa fa-backward">
                        </i> Go to Summary </a>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->

<script>
    $('#conf_pass').on('keyup', function () {
        var conf_pass = $(this).val();
        var pass = $('#pass').val();
        if (conf_pass == pass) {
            $('.check').html('<p class="label label-success"> Password Matched</p>');
        }
        else {
            $('.check').html('<p class="label label-danger"> Password not matched</p>')
        }
    });

    $('#email').on('change', function () {
        var email = $(this).val();
        var csrf = '<?=$this->security->get_csrf_hash()?>';
        $.ajax({
            url: '<?=base_url()?>home/check_email',
            type: 'post',
            data: {email: email, csrf: csrf},
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    $('#email_alert').html('<p class="label label-success">' + response.message + '</p>');
                }
                else {
                    $('#email_alert').html('<p class="label label-danger">' + response.message + '</p>');
                }
            },
            error: function (XHR, txtStatus, errorThrown) {
                console.log('Error: ' + errorThrown);
            }
        });
    });
</script>