<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="<?= base_url() ?>public/site/assets/lib/bootstrap/css/bootstrap.min.css"/>
    <style>
        th{
            text-align: center;
        }
        .h3{
            border-bottom: 1px solid #e3e3e3;
            position:static;
        }
    </style>

</head>
<body>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-6">
            <h3 class="h3">Order Details</h3>
            <h4><?=$order->f_name.' '.$order->l_name?></h4>
            <h5><strong>Order No:</strong> <?=$order->order_id?></h5>
            <h5><strong>Invoice No: </strong><?=$order->invoice_id?></h5>
            <h5><strong>Date :</strong>
                <?php
                $date=strtotime($order->created_at);
                echo date('d M Y',$date);
                ?>
            </h5>


        </div>
        <div class="col-sm-6">
            <h3 class="h3">Delivery/Billing Adddress</h3>
            <h5><strong>Address: </strong><?=$order->address_1?></h5>
            <h5><strong>District: </strong><?=$order->district_name?></h5>
            <h5><strong>Mobile: </strong><?=$order->mobile_phone?></h5>
            <?php if($order->home_phone){?>
                <h5><strong>Home: </strong><?=$order->home_phone?></h5>
            <?php }?>

        </div>

    </div>
</div>
<br>
<br>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>SN</th>
                <th>Product Name & Details</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0; foreach($product as $p){$i+=1;?>
                <tr>
                    <td><?=$i?></td>
                    <td><?=$p->product_name?></td>
                    <td><?=$p->qty?></td>
                    <td><?=$p->price?>/=</td>
                    <td><?=$p->subtotal?>/=</td>
                </tr>
            <?php }?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4"><strong class="pull-right">Total</strong></td>
                <td colspan="2"><strong class="total"><?=$total->subtotal?>/=</strong></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-sm-12">
        <p class="alert alert-info"><strong>Additional Message :</strong><?=$order->additional_message?></p>
    </div>
    <div class="col-sm-12">
        <a class="btn btn-warning btn-sm" onclick="window.print()">Print Order</a>
        <h3 class="label label-danger pull-right" >Powered By Eshop</h3>
    </div>
</div>
</body>
</html>