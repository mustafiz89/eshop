
<div class="columns-container">

    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">All Category</span>
        </div>
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block filter -->
                <div class="block left-module">
                    <p class="title_block">ক্যাটাগরি সিলেকশন</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-filter-price">
                            <!-- filter categgory -->
<!--                            <div class="layered_subtitle">CATEGORIES</div>-->
                            <div class="layered-content">
                                <ul class="check-box-list">
                                    <?php
                                    $model=new Category_model();
                                    $data['all_sub_category']=$model->selectAllActiveSubCategoryByCategoryId($category_id);
                                    foreach($data['all_sub_category'] as $s)
                                    {
                                    ?>
                                    <li>
                                        <a href="<?=base_url()?>home/product_by_sub_category/<?=$s->sub_category_id?>">
                                        <i class="fa fa-link"></i>
                                            <?=$s->sub_category_name?>
<!--                                            <span class="count">(10)</span>-->
                                        </a>
                                    </li>
                                    <?php  } ?>
                                </ul>
                            </div>
                            <!-- ./filter categgory -->
                            <!-- filter price -->
                            <div class="layered_subtitle">price</div>
                            <div class="layered-content slider-range">

                                <div data-label-reasult="Range:" data-min="0" data-max="500" data-unit="$" class="slider-range-price" data-value-min="50" data-value-max="350"></div>
                                <div class="amount-range-price">Range: $50 - $350</div>
                                <ul class="check-box-list">
                                    <li>
                                        <input type="checkbox" id="p1" name="cc" />
                                        <label for="p1">
                                            <span class="button"></span>
                                            $20 - $50<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="p2" name="cc" />
                                        <label for="p2">
                                            <span class="button"></span>
                                            $50 - $100<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="p3" name="cc" />
                                        <label for="p3">
                                            <span class="button"></span>
                                            $100 - $250<span class="count">(0)</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <!-- ./filter price -->



                            <!-- ./filter brand -->
                            <div class="layered_subtitle">brand</div>
                            <div class="layered-content filter-brand">
                                <ul class="check-box-list">
                                    <li>
                                        <input type="checkbox" id="brand1" name="cc" />
                                        <label for="brand1">
                                            <span class="button"></span>
                                            Channelo<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="brand2" name="cc" />
                                        <label for="brand2">
                                            <span class="button"></span>
                                            Mamypokon<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="brand3" name="cc" />
                                        <label for="brand3">
                                            <span class="button"></span>
                                            Pamperson<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="brand4" name="cc" />
                                        <label for="brand4">
                                            <span class="button"></span>
                                            Pumano<span class="count">(0)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="brand5" name="cc" />
                                        <label for="brand5">
                                            <span class="button"></span>
                                            AME<span class="count">(0)</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <!-- ./filter brand -->
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block filter  -->
            </div>
            <!-- ./left colunm -->


            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title">All Product</span>
                    </h2>
                    <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">

                        <?php
                        foreach($all_product as $p){
                        ?>
                        <li class="col-sx-12 col-sm-4">
                            <div class="product-container">
                                <div class="left-block">
                                    <a href="<?=base_url()?>home/product_details/<?=$p->product_id?>">
                                        <img class="img-responsive" alt="product" src="<?=base_url()?>public/upload/product/thumbnail/<?=$p->product_default_image?>" />
                                    </a>
                                    <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="#"></a>
                                    </div>
                                    <div class="add-to-cart">
                                        <a class="add-cart-product" title="Add to Cart" data-id="<?=$p->product_id?>" data-qty="1" >কার্টে যুক্ত করুন</a>
                                    </div>
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a target="_blank" href="<?=base_url()?>home/product_details/<?=$p->product_id?>"><?=$p->product_title?></a></h5>
                                    <div class="content_price">
                                        <span class="price product-price">৳ <?=$p->product_price?></span>
                                        <?php if($p->product_previous_price){
                                            echo '<span class="price product-price old-price" style="color:#E13300; font-weight: bold">৳ '.$p->product_previous_price.'</span>';
                                        }?>
                                    </div>
                                    <div class="info-orther">
<!--                                        <p>Item Code: #453217907</p>-->
                                        <?php if($p->discount>0){
                                            echo '<span class="discount" style="font-weight: bold">('.$p->discount.'% Discount)</span>';
                                        }?>
                                        <p class="availability">উপস্থিতি:
                                            <?php
                                            if($p->stock_status==1){
                                                echo '<span>স্টকে আছে</span>';
                                            }
                                            else{
                                                echo '<span style="color:#E13300">স্টক শেষ</span>';
                                            }
                                            ?>

                                        </p>
                                        <div class="product-desc">
                                            <?=$p->product_details?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            <ul class="pagination">
                                <li><?php echo $this->pagination->create_links(); ?></li>
                            </ul>
                        </nav>
                    </div>
<!--                    <div class="show-product-item">-->
<!--                        <select name="">-->
<!--                            <option value="">Show 18</option>-->
<!--                            <option value="">Show 20</option>-->
<!--                            <option value="">Show 50</option>-->
<!--                            <option value="">Show 100</option>-->
<!--                        </select>-->
<!--                    </div>-->
<!--                    <div class="sort-product">-->
<!--                        <select>-->
<!--                            <option value="">Product Name</option>-->
<!--                            <option value="">Price</option>-->
<!--                        </select>-->
<!--                        <div class="sort-product-icon">-->
<!--                            <i class="fa fa-sort-alpha-asc"></i>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script>

</script>