<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>
<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?=base_url() ?>shop/product/product_add_form" method="get">
                <input id="csrf" type="hidden" name=""
                       value="<?=$this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Select all category for add product</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/layout/notification') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Main Category Name:</label>

                            <div class="col-sm-4">
                                <select id="main_category_id" name="main_category_id" class="form-control input-md"
                                        required>
                                    <option value="">--SELECT--</option>
                                    <?php foreach ($main_category as $m) { ?>
                                        <option
                                            value="<?= $m->main_category_id ?>"><?= $m->main_category_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Category Name:</label>

                            <div class="col-sm-4">
                                <select id="category_id" name="category_id" class="form-control input-md" required>
                                    <option value="">--SELECT--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Sub Category</label>

                            <div class="col-sm-4">
                                <select id="sub_category_id" name="sub_category_id" class="form-control input-md"
                                        required>
                                    <option value="">--SELECT--</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-primary">Add Product in those category</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
    <!-- row -->
</div>
<script>

    $(function () {
        $('#main_category_id').on('change', function () {
            var id = $(this).val();
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/category/select_category_by_main_category_id',
                type: 'POST',
                data: {id: id, csrf: csrf},
                success: function (response) {
                    $('#category_id').html(response);
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });

        $('#category_id').on('change', function () {
            var category_id = $(this).val();
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/category/select_sub_category_by_category_id',
                type: 'POST',
                data: {id: category_id, csrf: csrf},
                success: function (response) {
                    $('#sub_category_id').html(response);
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });
    });

</script>