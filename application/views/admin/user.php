
<div class="pageheader">
    <h2><i class="fa fa-home"></i>Manage User</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="">Eshop</a></li>
            <li class="active">User</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" class="panel-close">×</a>
                        <a href="#" class="minimize">−</a>
                    </div><!-- panel-btns -->
<!--                    <h3 class="panel-title">User List</h3>-->
                    <button id="add-user" class="btn btn-success-alt btn-sm"><i class="fa fa-plus"></i> Add user</button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php $this->load->view('admin/layout/notification')?>
                       <div class="table-responsive">

                            <table class="table table-striped" id="user">
                                <thead>
                                <tr>
                                    <th width="8%">SN</th>
                                    <th>Full Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Image</th>
                                    <th width="11%">Action</th>
                                </tr>
                                </thead>
                               <tbody>
                               <?php $i=0; foreach($users as $user){ $i+=1;?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$user->full_name?></td>
                                        <td><?=$user->username?></td>
                                        <td><?=$user->email?></td>
                                        <td><?php
                                            if($user->role==1){
                                                echo "Admin";
                                            }
                                            else{
                                                echo "User";
                                            }
                                            ?>
                                        </td>
                                        <td><img src="<?=base_url().$user->image?>" height="50" width="50"></td>

                                        <td>
                                            <a data-id="<?=$user->user_id?>" class="edit-user btn btn-sm btn-primary-alt"><i class="fa fa-pencil"></i></a>
                                            <a href="<?=base_url()?>user/delete_user/<?=$user->user_id?>" class="delete btn btn-sm btn-danger-alt"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                               <?php }?>
                               </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- row -->
                </div><!-- panel-body -->
            </div><!-- panel -->
        </div><!-- col-sm-9 -->
    </div><!-- row -->
</div>
<!-- contentpanel -->

<!--Bootstrap Modal -->
<div class="modal fade userModal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="title" class="modal-title">Add User</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                 <input id="csrf" type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>"/>

                    <div class="form-group">
                        <div class="col-sm-8">
                            <?php $this->load->view('admin/layout/notification')?>
                            <input id="user_id" type="hidden"  name="user_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Full Name</label>
                        <div class="col-sm-8">
                            <input id="full_name" type="text"  class="form-control input-sm mb15"  placeholder="Full Name" name="full_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-8">
                            <input id="username" type="text"  class="form-control input-sm mb15"  placeholder="Username" name="username">                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password"  class="form-control input-sm mb15"  placeholder="Password" name="password">
                            <span style="color:#ff2222">Password must be at least 6 character</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-8">
                            <input type="password"  class="form-control input-sm mb15"  placeholder="Confirm Password" name="conf_password">                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                            <input id="email" type="email"  class="form-control input-sm mb15"  placeholder="Email" name="email">                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">User Role</label>
                        <div class="col-sm-3">
                            <select id="role" name="role" class="form-control input-sm">
                                <option value="">--SELECT--</option>
                                <option value="1">Admin</option>
                                <option value="2">User</option>
                            </select>
                        </div>
                    </div>
                    <div id="prev-image" class="form-group">
                        <label class="col-sm-3 control-label">Previous Image</label>
                        <div class="col-sm-6">
                            <img id="img" src="" width="80" height="80">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">User Image</label>
                        <div class="col-sm-6">
                            <input id="file" type="file"  class="form-control input-sm mb15" name="image" accept="image/*" >
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-sm-3 col-sm-offset-3">
                           <button id="button" type="submit" class="btn btn-sm btn-block btn-primary">Save User</button>
                        </div>
                    </div>

                </form>
            </div>
<!--            <div class="modal-footer">-->
<!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
<!--            </div>-->
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script>
    $('#user').dataTable({
        "sPaginationType": "full_numbers"
    });

//    modal for  add user
    $('#add-user').on("click",function(){
        $('.form-horizontal')[0].reset();
        $('.form-horizontal').attr('action','<?=base_url('dashboard/user/save_user')?>');
        $('#button').html('Save user');
        $('#title').html('Add User');
        $('#prev-image').hide();
        $('.userModal').modal("show");
    });

    // Modal for edit user
    $('.edit-user').on("click",function(){
        $('#prev-image').show();
        $('.form-horizontal')[0].reset();
        $('#button').html('Update user');
        $('#title').html('Edit User');
        var user_id=$(this).attr('data-id');
        var csrf=$('#csrf').val();
        $.ajax({
            url:"<?=base_url('dashboard/user/select_user')?>",
            type:"POST",
            data:{user_id:user_id,csrf:csrf},
            dataType:'json',
            success: function(response){
                $('.form-horizontal').attr('action','<?=base_url('dashboard/user/update_user')?>');
                $('#user_id').val(response.user_id);
                $('#full_name').val(response.full_name);
                $('#username').val(response.username);
                $('#email').val(response.email);
                $('#role').val(response.role);
                $('#img').attr('src','<?=base_url()?>'+response.image);
                console.log(response.image);
                $('.userModal').modal("show");
            },
            error:function(XHR,txtStatus,errorThrown){
                alert('Error: '+errorThrown);
            }
        });
    });

    $('.delete').on('click',function(){
        var check=confirm("Want to delete it?");
        if(check){
            return true;
        }
        else{
            return false;
        }
    });

</script>