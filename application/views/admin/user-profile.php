<div class="pageheader">
    <h2><i class="fa fa-home"></i>Manage profile</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="">Eshop</a></li>
            <li class="active">Profile</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" class="panel-close">×</a>
                        <a href="#" class="minimize">−</a>
                    </div><!-- panel-btns -->
                    <h3 class="panel-title">Profile</h3>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="form-horizontal" action="<?=base_url()?>dashboard/user/update_user_profile" method="post" enctype="multipart/form-data">
                            <input id="csrf" type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>"/>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <?php $this->load->view('admin/layout/notification')?>
                                    <input id="user_id" type="hidden"  name="user_id" value="<?=$user->user_id?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Full Name</label>
                                <div class="col-sm-8">
                                    <input id="full_name" type="text"  class="form-control input-sm mb15"  placeholder="Full Name" name="full_name" value="<?=$user->full_name?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-8">
                                    <input id="username" type="text"  class="form-control input-sm mb15"  placeholder="Username" name="username" value="<?=$user->username?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-8">
                                    <input type="password"  class="form-control input-sm mb15"  placeholder="Password" name="password" value="">
                                    <span style="color:#ff2222">Password must be at least 6 character</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Confirm Password</label>
                                <div class="col-sm-8">
                                    <input type="password"  class="form-control input-sm mb15"  placeholder="Confirm Password" name="conf_password">                        </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input id="email" type="email"  class="form-control input-sm mb15"  placeholder="Email" name="email" value="<?=$user->email?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">User Role</label>
                                <div class="col-sm-3">
                                    <select id="role" name="role" class="form-control input-sm" required>
                                        <option value="">--SELECT--</option>
                                        <option value="1">Admin</option>
                                        <option value="2">User</option>
                                    </select>
                                </div>
                            </div>
                            <div id="prev-image" class="form-group">
                                <label class="col-sm-3 control-label">Previous Image</label>
                                <div class="col-sm-6">
                                    <img id="img" src="<?=base_url().$user->image?>" width="80" height="80">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">User Image</label>
                                <div class="col-sm-6">
                                    <input id="file" type="file"  class="form-control input-sm mb15" name="image" accept="image/*" >
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-3 col-sm-offset-3">
                                    <button id="button" type="submit" class="btn btn-sm btn-block btn-primary">Update User</button>
                                </div>
                            </div>

                        </form>
                    </div><!-- row -->
                </div><!-- panel-body -->
            </div><!-- panel -->
        </div><!-- col-sm-9 -->
    </div><!-- row -->
</div>