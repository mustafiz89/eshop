<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>
<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/category/manage_sub_category"
                  method="get">
                <input id="csrf" type="hidden" value="<?= $this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Select Data</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Main Category Name:</label>

                            <div class="col-sm-3">
                                <select name="main_category_id" class="main_category_id form-control input-sm" required>
                                    <option value="">--SELECT--</option>
                                    <?php foreach ($main_category as $m) { ?>
                                        <option
                                            value="<?= $m->main_category_id ?>"><?= $m->main_category_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Category Name:</label>

                            <div class="col-sm-3">
                                <select id="" name="category_id" class="category_id form-control input-sm" required>
                                    <option value="">--SELECT--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-sm btn-primary">Select Data</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
    <!-- row -->

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" class="panel-close">×</a>
                        <a href="#" class="minimize">−</a>
                    </div>
                    <!-- panel-btns -->
                    <h3 class="panel-title">View Sub Category</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php $this->load->view('admin/layout/notification') ?>
                        <div class="table-responsive">

                            <table class="table table-striped" id="main-category">
                                <thead>
                                <tr>
                                    <th width="8%">SN</th>
                                    <th>Sub Category</th>
                                    <th>Main Category</th>
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th width="">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($sub_category)) {
                                    $i = 0;
                                    foreach ($sub_category as $s) {
                                        $i += 1?>

                                        <tr>
                                            <td><?= $i ?></td>

                                            <td><?= $s->sub_category_name ?></td>
                                            <td><?= $s->main_category_name ?></td>
                                            <td><?= $s->category_name ?></td>
                                            <td>
                                                <?php if ($s->sub_category_status == 1)
                                                    echo '<span class="label label-success">Show</span>';
                                                else echo '<span class="label label-danger">Hidden</span>';
                                                ?>
                                            </td>
                                            <td>
                                                <a data-id="<?= $s->sub_category_id ?>"
                                                   class="edit-data btn btn-sm btn-primary-alt"><i
                                                        class="fa fa-pencil"></i></a>
                                                <a href="<?= base_url() ?>shop/category/delete_sub_category/<?= $s->category_id ?>"
                                                   class="delete btn btn-sm btn-danger-alt"><i
                                                        class="fa fa-trash-o"></i></a>

                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- table-responsive -->
                    </div>
                    <!-- row -->
                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col-sm-9 -->
    </div>
</div>
<!--Bootstrap Modal -->
<div class="modal fade categoryModal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/category/update_sub_category"
                      method="post" enctype="multipart/form-data">
                    <input id="csrf" type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                           value="<?= $this->security->get_csrf_hash() ?>"/>
                    <input id="sub_category_id" type="hidden" name="sub_category_id">

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Sub Category Name</label>

                        <div class="col-sm-8">
                            <input id="sub_category_name" type="text" class="form-control input-sm"
                                   placeholder="Sub Category Name" name="sub_category_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Main Category</label>

                        <div class="col-sm-4">
                            <select name="main_category_id" class="main_category_id form-control input-sm" required>
                                <option value="">--SELECT--</option>
                                <?php foreach ($main_category as $d) { ?>
                                    <option value="<?= $d->main_category_id ?>"><?= $d->main_category_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Category</label>

                        <div class="col-sm-4">
                            <select id="" name="category_id" class="category_id form-control input-sm" required>
                                <option value="">--SELECT--</option>
                                <?php foreach ($category as $d) { ?>
                                    <option value="<?= $d->category_id ?>"><?= $d->category_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Category Status</label>

                        <div class="col-sm-3">
                            <select id="sub_category_status" name="sub_category_status" class="form-control input-sm"
                                    required>
                                <option value="">--SELECT--</option>
                                <option value="1">SHOW</option>
                                <option value="0">HIDDEN</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-4">
                            <button id="button" type="submit" class="btn btn-sm btn-block btn-primary">Save Data
                            </button>
                        </div>
                    </div>

                </form>
            </div>
            <!--            <div class="modal-footer">-->
            <!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <!--            </div>-->
        </div>
        <!-- modal-content -->
    </div>
    <!-- modal-dialog -->
</div><!-- modal -->


<script>
    $(function () {
        $('#main-category').dataTable({
            "sPaginationType": "full_numbers"
        });

        $('.main_category_id').on('change', function () {
            var id = $(this).val();
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/category/select_category_by_main_category_id',
                type: 'POST',
                data: {id: id, csrf: csrf},
                success: function (response) {
                    $('.category_id').html(response);
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });

        $('.delete').on('click', function () {
            var check = confirm("Want to delete it?");
            if (check) {
                return true;
            }
            else {
                return false;
            }
        });
    });

    // Modal for edit Data
    $('.edit-data').on("click", function () {
        $('.form-horizontal')[0].reset();
        $('#button').html('Update Data');
        $('#title').html('Sub Category');
        var sub_category_id = $(this).attr('data-id');
        var csrf = $('#csrf').val();

        $.ajax({
            url: "<?=base_url('shop/category/select_sub_category_by_id')?>",
            type: "POST",
            data: {sub_category_id: sub_category_id, csrf: csrf},
            dataType: 'json',
            success: function (response) {

                $('#sub_category_id').val(response.sub_category_id);
                $('.category_id').val(response.category_id);
                $('.main_category_id').val(response.main_category_id);
                $('#sub_category_name').val(response.sub_category_name);
                $('#sub_category_status').val(response.sub_category_status);
                $('.categoryModal').modal("show");
            },
            error: function (XHR, txtStatus, errorThrown) {
                alert('Error: ' + errorThrown);
            }
        });
    });
</script>