
<div class="pageheader">
    <h2><i class="fa fa-home"></i><?=$header?></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?=$site?></a></li>
            <li class="active"><?=$active?></li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" class="panel-close">×</a>
                        <a href="#" class="minimize">−</a>
                    </div><!-- panel-btns -->
                    <!--                    <h3 class="panel-title">User List</h3>-->
                    <button id="add-data" class="btn btn-success-alt btn-sm"><i class="fa fa-plus"></i> Add Category</button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php $this->load->view('admin/layout/notification')?>
                        <div class="table-responsive">

                            <table class="table table-striped" id="main-category">
                                <thead>
                                <tr>
                                    <th width="8%">SN</th>
                                    <th>Category</th>
                                    <th>Main Category</th>
                                    <th>Status</th>
                                    <th>Featured Option</th>
                                    <th width="">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=0; foreach($category as $c){ $i+=1?>
                                    <tr>
                                        <td><?=$i?></td>

                                        <td><?=$c->category_name?></td>
                                        <td><?=$c->main_category_name?></td>
                                        <td>
                                            <?php if($c->category_status==1)
                                                echo '<span class="label label-success">Show</span>';
                                            else echo '<span class="label label-danger">Hidden</span>';
                                            ?>
                                        </td>
                                        <td>
                                            <a target="_blank" href="<?=base_url()?>shop/category/featured_category/<?=$c->category_id?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Featured Option</a>
                                           <?php if($c->widget_status==1){?>
                                            <a href="<?=base_url()?>shop/category/hide_featured_category/<?=$c->category_id?>" class="btn btn-warning btn-sm"><i class="fa fa-info-circle"></i> Hide Option</a>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <a data-id="<?=$c->category_id?>" class="edit-data btn btn-sm btn-primary-alt" ><i class="fa fa-pencil"></i></a>
                                            <a title="Delete Category" href="<?=base_url()?>shop/category/delete_category/<?=$c->category_id?>" class="delete btn btn-sm btn-danger-alt"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- row -->
                </div><!-- panel-body -->
            </div><!-- panel -->
        </div><!-- col-sm-9 -->
    </div><!-- row -->
</div>
<!-- contentpanel -->

<!--Bootstrap Modal -->
<div class="modal fade categoryModal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <input id="csrf" type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>"/>

                    <div class="form-group">
                        <div class="col-sm-8">
                            <input id="category_id" type="hidden"  name="category_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Category Name</label>
                        <div class="col-sm-8">
                            <input id="category_name" type="text"  class="form-control input-sm"  placeholder="Category Name" name="category_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Main Category</label>
                        <div class="col-sm-4">
                            <select id="main_category_id" name="main_category_id" class="form-control input-sm" required>
                                <option value="">--SELECT--</option>
                                <?php foreach($main_category as $d){?>
                                    <option value="<?=$d->main_category_id?>"><?=$d->main_category_name?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Category Status</label>
                        <div class="col-sm-3">
                            <select id="category_status" name="category_status" class="form-control input-sm" required>
                                <option value="">--SELECT--</option>
                                <option value="1">SHOW</option>
                                <option value="0">HIDDEN</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3 col-sm-offset-4">
                            <button id="button" type="submit" class="btn btn-sm btn-block btn-primary">Save Data</button>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script>
    $('#main-category').dataTable({
        "sPaginationType": "full_numbers"
    });

    //    modal for  add DATA
    $('#add-data').on("click",function(){
        $('.form-horizontal')[0].reset();
        $('.form-horizontal').attr('action','<?=base_url('shop/category/save_category')?>');
        $('#button').html('Save Data');
        $('#title').html('Category');
        $('.categoryModal').modal("show");
    });

    // Modal for edit Data
    $('.edit-data').on("click",function(){
        $('.form-horizontal')[0].reset();
        $('#button').html('Update Data');
        $('#title').html('Category');
        var category_id=$(this).attr('data-id');
        var csrf=$('#csrf').val();
        $.ajax({
            url:"<?=base_url('shop/category/select_category_by_id')?>",
            type:"POST",
            data:{category_id:category_id,csrf:csrf},
            dataType:'json',
            success: function(response){
                $('.form-horizontal').attr('action','<?=base_url('shop/category/update_category')?>');
                $('#category_id').val(response.category_id);
                $('#main_category_id').val(response.main_category_id);
                $('#category_name').val(response.category_name);
                $('#category_status').val(response.category_status);
                $('.categoryModal').modal("show");
            },
            error:function(XHR,txtStatus,errorThrown){
                alert('Error: '+errorThrown);
            }
        });
    });

    $('.delete').on('click',function(){
        var check=confirm("Want to delete it?");
        if(check){
            return true;
        }
        else{
            return false;
        }
    });

</script>