<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/product/manage_product"
                  method="get">
                <input id="csrf" type="hidden" name=""
                       value="<?= $this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Select all category for view product list</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Main Category Name:</label>

                            <div class="col-sm-3">
                                <select id="main_category_id" name="main_category_id" class="form-control input-md"
                                        required>
                                    <option value="">--SELECT--</option>
                                    <?php foreach ($main_category as $m) { ?>
                                        <option
                                            value="<?= $m->main_category_id ?>"><?= $m->main_category_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Category Name:</label>

                            <div class="col-sm-3">
                                <select id="category_id" name="category_id" class="form-control input-md" required>
                                    <option value="">--SELECT--</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Sub Category</label>

                            <div class="col-sm-3">
                                <select id="sub_category_id" name="sub_category_id" class="form-control input-md"
                                        required>
                                    <option value="">--SELECT--</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-primary btn-md">Select category</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/product/manage_product"
                  method="get">
                <input id="csrf" type="hidden" name=""
                       value="<?= $this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">View product list</h4>
                    </div>
                    <div class="panel-body">
                        <?php $this->load->view('admin/layout/notification') ?>
                        <div class="table-responsive">
                            <table class="table table-striped" id="product">
                                <thead>
                                <tr>
                                    <th width="8%">SN</th>
                                    <th style="width:20%">Product Name</th>
                                    <th>Main Category</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Default Img</th>
                                    <th>Other Img</th>
                                    <th>Status</th>
                                    <th width="">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($product)) {
                                    $i = 0;
                                    foreach ($product as $p) {
                                        $i += 1?>
                                        <tr>
                                            <td><?= $i ?></td>

                                            <td><?= $p->product_title ?></td>
                                            <td><?= $p->main_category_name ?></td>
                                            <td><?= $p->category_name ?></td>
                                            <td><?= $p->sub_category_name ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>public/upload/product/<?= $p->product_default_image ?>"
                                                   data-rel="prettyPhoto">
                                                    <img
                                                        src="<?= base_url() ?>public/upload/product/thumbnail/<?=$p->product_default_image ?>"
                                                        class="img-responsive" alt="" height="60" width="60"/>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="" class="btn btn-primary btn-sm"><i class="fa fa-street-view"></i> View all Image</a>
                                            </td>
                                            <td>
                                                <?php if ($p->product_status == 1)
                                                    echo '<span class="label label-success">Show</span>';
                                                else echo '<span class="label label-danger">Hidden</span>';
                                                ?>
                                            </td>

                                            <td>
                                                <a data-id="<?=$p->product_id?>" class="view-data btn btn-sm btn-info-alt"><i
                                                        class="fa fa-search"></i></a>
                                                <a target="_blank" href="<?= base_url() ?>shop/product/edit_product/<?=$p->product_id?>"  class="edit-data btn btn-sm btn-primary-alt"><i
                                                        class="fa fa-pencil"></i></a>
                                                <a href="<?= base_url() ?>shop/product/delete_product/<?=$p->product_id?>"
                                                   class="delete btn btn-sm btn-danger-alt"><i
                                                        class="fa fa-trash-o"></i></a>

                                            </td>
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- table-responsive -->
                    </div>
                    <!-- row -->
                </div>
                <!-- panel-body -->
        </div>
        <!-- panel-default -->
    </div>
</div>
<!--Bootstrap Modal -->
<div class="modal fade productModal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="title" class="modal-title">View Product Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th style="width:30%">Product Title</th>
                        <td id="product_title"></td>
                    </tr>
                    <tr>
                        <th style="">Product Price</th>
                        <td><span id="product_price"></span> /=</td>
                    </tr>
                    <tr>
                        <th style="">Discount</th>
                        <td><span id="discount"></span> %</td>
                    </tr>
                    <tr>
                        <th style="">Product previous price</th>
                        <td><span id="previous_price"></span> /=</td>
                    </tr>
                    <tr>
                        <th style="width:30%">Features</th>
                        <td id="product_details"></td>
                    </tr>
                    <tr>
                        <th style="width:30%">Technical</th>
                        <td id="technical"></td>
                    </tr>
                    <tr>
                        <th style="width:30%">Support</th>
                        <td id="support"></td>
                    </tr>
                    <tr>
                        <th style="width:30%">Manual</th>
                        <td id="manual"></td>
                    </tr>
                    <tr>
                        <th style="width:30%">Default Image</th>
                        <td>
                            <img id="default_image" src="" width="200" height="200" />
                        </td>
                    </tr>
                    <tr>
                        <th style="width:30%">Other Images</th>
                        <td>Name</td>
                    </tr>
                </table>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

<script>

    $('#product').dataTable({
        "sPaginationType": "full_numbers"
    });

    $(function () {
        $('#main_category_id').on('change', function () {
            var id = $(this).val();
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/category/select_category_by_main_category_id',
                type: 'POST',
                data: {id: id, csrf: csrf},
                success: function (response) {
                    $('#category_id').html(response);
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });

        $('#category_id').on('change', function () {
            var category_id = $(this).val();
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/category/select_sub_category_by_category_id',
                type: 'POST',
                data: {id: category_id, csrf: csrf},
                success: function (response) {
                    $('#sub_category_id').html(response);
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });

        $('.view-data').on('click',function(){
            var id = $(this).attr('data-id');
            var csrf = $('#csrf').val();
            $.ajax({
                url: '<?=base_url()?>shop/product/select_product_by_product_id',
                type: 'POST',
                data: {product_id: id, csrf: csrf},
                dataType:'json',
                success: function (response) {
                    $('#product_title').html(response.product_title);
                    $('#product_price').html(response.product_price);
                    $('#discount').html(response.discount);
                    $('#previous_price').html(response.product_previous_price);
                    $('#product_details').html(response.product_details);
                    $('#technical').html(response.technical);
                    $('#support').html(response.support);
                    $('#manual').html(response.manual);
                    $('#default_image').attr('src','<?=base_url()?>public/upload/product/thumbnail/'+response.product_default_image);
                    $('.productModal').modal('show');
                },
                error: function (XHR, txtStatus, errorThrown) {
                    alert('Error: ' + errorThrown);
                }
            });
        });


        $('.delete').on('click',function(){
            var check=confirm("Want to delete it?");
            if(check){
                return true;
            }
            else{
                return false;
            }
        });
    });

</script>