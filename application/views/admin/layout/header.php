<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <title>E-Shop</title>

    <link href="<?=base_url()?>public/admin/css/style.default.css" rel="stylesheet">
    <link href="<?=base_url()?>public/admin/css/jquery.datatables.css" rel="stylesheet">
    <link href="<?=base_url()?>public/admin/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?=base_url()?>public/admin/css/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" href="<?=base_url()?>public/admin/css/prettyPhoto.css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>public/admin/js/html5shiv.js"></script>
    <script src="<?=base_url()?>public/admin/js/respond.min.js"></script>
    <![endif]-->

    <script src="<?=base_url()?>public/admin/js/jquery-1.11.1.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/jquery.datatables.min.js"></script>
<!--    <script src="--><?//=base_url()?><!--public/admin/js/select2.min.js"></script>-->
    <script src="<?=base_url()?>public/admin/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/jquery-ui-1.10.3.min.js"></script>

    <script src="<?=base_url()?>public/admin/js/modernizr.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/wysihtml5-0.3.0.min.js"></script>
    <script src="<?=base_url()?>public/admin/js/bootstrap-wysihtml5.js"></script>
    <script src="<?=base_url()?>public/admin/js/jquery.prettyPhoto.js"></script>
    <script src="<?=base_url()?>public/admin/js/custom.js"></script>

</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="leftpanel">

        <div class="logopanel">
            <h1><span>[</span> E-Shop <span>]</span></h1>
        </div><!-- logopanel -->

        <div class="leftpanelinner">

            <!-- This is only visible to small devices -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media userlogged">
                    <img alt="" src="<?=base_url().$this->session->userdata('image')?>" class="media-object">
                    <div class="media-body">
                        <h4><?=$this->session->userdata('full_name')?></h4>
                    </div>
                </div>

                <h5 class="sidebartitle actitle">Account</h5>
                <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                    <li><a href="<?=base_url()?>dashboard/user/edit_user_profile/<?=$this->session->userdata('user_id')?>"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                    <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
                    <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
                    <li><a href="<?=base_url()?>auth/admin/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>

            <h5 class="sidebartitle">Navigation</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket">
                <li class=""><a href="<?=base_url()?>auth/admin"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class=""><a href="<?=base_url()?>shop/category/manage_main_category"><i class="fa fa-edit"></i> <span>Menu Category</span></a></li>
                <li class=""><a href="<?=base_url()?>shop/category/manage_category"><i class="fa fa-edit"></i> <span>Category</span></a></li>
                <li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Sub Category</span></a>
                    <ul class="children">
                        <li><a href="<?=base_url()?>shop/category/sub_category_form"><i class="fa fa-caret-right"></i> Add Sub-category</a></li>
                        <li><a href="<?=base_url()?>shop/category/manage_sub_category"><i class="fa fa-caret-right"></i> View Sub-category</a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Product</span></a>
                    <ul class="children">
                        <li><a href="<?=base_url()?>shop/product/select_all_category_for_add_product"><i class="fa fa-caret-right"></i> Add Product</a></li>
                        <li><a href="<?=base_url()?>shop/product/manage_product"><i class="fa fa-caret-right"></i> View Product</a></li>
                    </ul>
                </li>
                <li class="nav-parent"><a href="#"><i class="fa fa-edit"></i> <span>Order</span></a>
                    <ul class="children">
                        <li><a href="<?=base_url()?>shop/order/view_pending_order"><i class="fa fa-caret-right"></i> View Pending Order</a></li>
                        <li><a href="<?=base_url()?>shop/order/view_complete_order"><i class="fa fa-caret-right"></i> View Complete Order</a></li>
                        <li><a href="<?=base_url()?>shop/order/view_instant_order"><i class="fa fa-caret-right"></i> View Instant Order</a></li>
                    </ul>
                </li>

                <?php if($this->session->userdata('role')==1){?>
                <li ><a href="<?=base_url()?>dashboard/user"><i class="fa fa-user"></i> <span>User</span></a></li>
                <?php }?>
            </ul>

            <div class="infosummary">
                <h5 class="sidebartitle">Information Summary</h5>
                <ul>
                    <li>
                        <div class="datainfo">
                            <span class="text-muted">Daily Traffic</span>
                            <h4>None</h4>
                        </div>
                        <div id="sidebar-chart" class="chart"></div>
                    </li>
                    <li>
                        <div class="datainfo">
                            <span class="text-muted">Average Users</span>
                            <h4>None</h4>
                        </div>
                        <div id="sidebar-chart2" class="chart"></div>
                    </li>
                    <li>
                        <div class="datainfo">
                            <span class="text-muted">Disk Usage</span>
                            <h4>None</h4>
                        </div>
                        <div id="sidebar-chart3" class="chart"></div>
                    </li>
                    <li>
                        <div class="datainfo">
                            <span class="text-muted">CPU Usage</span>
                            <h4>None</h4>
                        </div>
                        <div id="sidebar-chart4" class="chart"></div>
                    </li>
                    <li>
                        <div class="datainfo">
                            <span class="text-muted">Memory Usage</span>
                            <h4>None</h4>
                        </div>
                        <div id="sidebar-chart5" class="chart"></div>
                    </li>
                </ul>
            </div><!-- infosummary -->

        </div><!-- leftpanelinner -->
    </div><!-- leftpanel -->

    <div class="mainpanel">

        <div class="headerbar">

            <a class="menutoggle"><i class="fa fa-bars"></i></a>

            <div class="header-right">
                <ul class="headermenu">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="images/photos/loggeduser.png" alt="" />
                                <?=$this->session->userdata('full_name')?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                <li><a href="<?=base_url()?>dashboard/user/edit_user_profile/<?=$this->session->userdata('user_id')?>"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li>
                                <li><a href="<?=base_url()?>auth/admin/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div><!-- header-right -->

        </div><!-- headerbar -->