<?php
$er=$this->session->userdata('success');
if($er)
{
?>
<h5 id="alert" class="alert alert-success">
    <?php
    echo $er.'</h5>';
    $this->session->unset_userdata('success');
    }
    ?>
    <?php
    $er=$this->session->userdata('error');
    if($er)
    {
    ?>
    <h5 id="alert" class="alert alert-danger">
        <?php
        echo $er.'</h5>';
        $this->session->unset_userdata('error');
        }
        ?>
        <?php
        $er=$this->session->userdata('warning');
        if($er)
        {
        ?>
        <h5 id="alert" class="alert alert-warning">
            <?php
            echo $er.'</h5>';
            $this->session->unset_userdata('warning');
            }
            ?>
            <?php
            $er=$this->session->userdata('info');
            if($er)
            {
            ?>
            <h5 id="alert" class="alert alert-info">
                <?php
                echo $er.'</h5>';
                $this->session->unset_userdata('info');
                }
                ?>
