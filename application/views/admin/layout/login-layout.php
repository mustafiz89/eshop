<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?=base_url()?>public/admin/images/favicon.png" type="image/png">

    <title>Super shop</title>

    <link href="<?=base_url()?>public/admin/css/style.default.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url()?>public/admin/js/html5shiv.js"></script>
    <script src="<?=base_url()?>public/admin/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">


<section>

    <div class="signinpanel">

        <div class="row">

            <div class="col-md-7">

                <div class="signin-info">
                    <div class="logopanel">
                        <h1><span>[</span> Super Shop <span>]</span></h1>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>

                    <h5><strong>Welcome to Super shop</strong></h5>
                    <div class="mb20"></div>
<!--                    <strong>Not a member? <a>Sign Up</a></strong>-->
                </div><!-- signin0-info -->

            </div><!-- col-sm-7 -->

            <div class="col-md-5">

                <form method="post" action="<?=base_url('auth/login/check_credential')?>">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>"/>
                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>
                    <?php $this->load->view('admin/layout/notification')?>
                    <input type="text" class="form-control uname" name="username" placeholder="Username" />
                    <input type="password" class="form-control pword" name="password" placeholder="Password" />
                    <a href="#"><small>Forgot Your Password?</small></a>
                    <button class="btn btn-success btn-block">Sign In</button>

                </form>
            </div><!-- col-sm-5 -->

        </div><!-- row -->

        <div class="signup-footer">
            <div class="pull-left">
                Super shop &copy; 2016. All Rights Reserved.
            </div>
            <div class="pull-right">
                Developed By: <a href="http://mustafiz.info/" target="_blank">Mustafizur Rahman</a>
            </div>
        </div>

    </div><!-- signin -->

</section>


<script src="<?=base_url()?>public/admin/public/admin/js/jquery-1.11.1.min.js"></script>
</body>
</html>
