<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal" action="<?=base_url() ?>shop/category/upload_featured_category_image"
                  method="post" enctype="multipart/form-data">
                <input id="csrf" type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                       value="<?=$this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Featured category Image</h4>
                    </div>
                    <div class="panel-body">
                          <input type="hidden" name="category_id" value="<?=$category_id?>">
                        <div class="form-group">
                                <?php
                                $er=$this->session->userdata('info');
                                if($er)
                                {
                                ?>
                                <h5 id="info" class="alert alert-info">
                                    <?php
                                    echo $er.'</h5>';
                                    $this->session->unset_userdata('info');
                                    }
                                    ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Widget Image</label>
                            <div class="col-sm-3">
                                <input type="file" name="widget_image[]" accept="image/*" multiple>
                                <span>Image size should be 386*500px</span>
                                <br>
                                        <button class="btn btn-primary btn-sm">Upload Image</button><br><br>

                            </div>
                            <div id="list" class="col-sm-3 col-sm-offset-1">
                                <p>Image list</p>
                                <table class="table table-bordered">
                                    <?php $i=0; foreach($widget_image as $w){$i+=1?>
                                        <tr>
                                            <td><?=$i?></td>
                                            <td><?=$w->widget_image?></td>
                                            <td><a href="<?=base_url()?>shop/category/delete_featured_image/<?=$w->category_image_id?>/<?=$category_id?>" class="delete btn btn-danger-alt btn-xs"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                    <?php }?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/category/save_featured_category"
                  method="post">
                <input id="csrf" type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                       value="<?= $this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Featured category option</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/layout/notification') ?>
                                <input type="hidden" name="category_id" value="<?=$category_id?>"
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Widget Title</label>
                            <div class="col-sm-5">
                                <input type="text" name="widget_title" class="form-control"
                                       placeholder="Widget Title" value="<?=$category->widget_title?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Widget Color</label>
                            <div class="col-sm-3">
                                <div id="cp2" class="input-group colorpicker-component">
                                    <input type="text" name="widget_color" value="<?=$category->widget_color?>" class="form-control"/>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-primary btn-sm">Save Featured Option</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form
        </div>
        <!-- col-sm-9 -->
    </div>
    <!-- row -->
</div>
<script>
    $(function () {
        $('#cp2').colorpicker();
        $('.delete').on('click',function(){
            var check=confirm("Want to delete it?");
            if(check){
                return true;
            }
            else{
                return false;
            }
        });
    });


</script>
