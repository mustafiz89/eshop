<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?= base_url() ?>shop/product/update_product"
                  method="post" enctype="multipart/form-data">
                <input id="csrf" type="hidden" name="<?= $this->security->get_csrf_token_name() ?>"
                       value="<?= $this->security->get_csrf_hash() ?>"/>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Add product</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/layout/notification') ?>
                                <input type="hidden" name="product_id" value="<?= $product->product_id ?>">
                                <input type="hidden" class="form-control" value="<?= $product->main_category_id ?>"
                                       name="main_category_id">
                                <input type="hidden" class="form-control" value="<?= $product->category_id ?>"
                                       name="category_id">
                                <input type="hidden" class="form-control" value="<?= $product->sub_category_id ?>"
                                       name="sub_category_id">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Title</label>

                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Product Title" name="product_title"
                                       required value="<?= $product->product_title ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Price</label>

                            <div class="col-sm-2">
                                <input class="form-control" type="text" placeholder="0.0" name="product_price"
                                       required value="<?= $product->product_price ?>">
                            </div>
                            <div class="col-sm-1">
                                <p style="font-size: 30px;padding-top:10px">৳</p>
                            </div>
                        </div>
                        <div id="previous_price" class="form-group ">
                            <label style="color:#008855" class="col-sm-3 control-label">Previous Price</label>

                            <div class="col-sm-2">
                                <input class="form-control" id="previous_price_text" type="text" placeholder="0.0"
                                       name="product_previous_price" value="<?= $product->product_previous_price ?>">
                            </div>
                            <div class="col-sm-1">
                                <p style="font-size: 30px;padding-top:10px">৳</p>
                            </div>
                        </div>

                        <div class="form-group discount">
                            <label class="col-sm-3 control-label">Discount</label>

                            <div class="col-sm-2">
                                <input class="form-control" type="text" placeholder="0.0" name="discount"
                                       value="<?= $product->discount ?>">
                            </div>
                            <div class="col-sm-1">
                                <p style="font-size: 30px;padding-top:10px">%</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Avialibility</label>

                            <div class="col-sm-2">
                                <select id="" name="stock_status" class="form-control input-md"
                                        required>
                                    <!--                                    <option value="">--SELECT--</option>-->
                                    <option value="1">In Stock</option>
                                    <option value="2">Out of Stock</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Condition</label>

                            <div class="col-sm-2">
                                <select id="" name="product_condition" class="form-control input-md"
                                        required>
                                    <!--                                    <option value="">--SELECT--</option>-->
                                    <option value="1">New</option>
                                    <option value="2">Brand New</option>
                                    <option value="4">Used</option>
                                    <option value="3">Old</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Status</label>

                            <div class="col-sm-2">
                                <select id="" name="product_status" class="form-control input-md"
                                        required>
                                    <option value="1">SHOW</option>
                                    <option value="0">HIDDEN</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Previous image</label>

                            <div class="col-sm-4">
                                <img
                                    src="<?= base_url() ?>public/upload/product/thumbnail/<?= $product->product_default_image ?>"
                                    height="200" width="200">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product New Default image</label>

                            <div class="col-sm-4">
                                <input type="file" name="product_default_image" accept="image/*">

                                <p>Image size should be 850*1036</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Details</label>

                            <div class="col-sm-8">
                                <textarea name="product_details" id="wysiwyg"
                                          placeholder="Enter product details here..." class="form-control"
                                          rows="15"><?= $product->product_details ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Technical Specification</label>
                            <div class="col-sm-8">
                                <textarea name="technical" id="technical" placeholder="Enter Technical specification here..." class="form-control" rows="15"><?=$product->technical?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Support</label>
                            <div class="col-sm-8">
                                <textarea name="support" id="support" placeholder="Enter support details here..." class="form-control" rows="15"><?=$product->support?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Manual</label>
                            <div class="col-sm-8">
                                <textarea name="manual" id="manual" placeholder="Enter Manual details here..." class="form-control" rows="15"><?=$product->manual?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-primary">Update Product</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
</div>
<script>
    $(function () {
        $('#wysiwyg').wysihtml5({color: true, html: true});
//        $('#previous_price').hide();
//        $('.discount').show();
//        $('#checkboxPrimary').on('change',function(){
//            var val=$(this).attr('checked');
//            if(val!=='checked'){
//                $('#previous_price').hide();
//                $('.discount').show();
//
//            }
//            else{
//                $('#previous_price').show();
//                $('.discount').hide();
//
//            }
//        });
    })

</script>