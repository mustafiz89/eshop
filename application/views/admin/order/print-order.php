<!DOCTYPE html>
<html>
<head>
    <link href="<?=base_url()?>public/admin/css/style.default.css" rel="stylesheet">

<!--    <link rel="stylesheet" type="text/css"-->
<!--          href="--><?//= base_url() ?><!--public/site/assets/lib/bootstrap/css/bootstrap.min.css"/>-->
    <style>
        th{
            text-align: center;
        }
        .h3{
            border-bottom: 1px solid #e3e3e3;
            position:static;
        }
    </style>

</head>
<body>
<div class="pageheader">
    <h2><i class="fa fa-th-large"></i> Eshop E-commerce <span>We care our customer</span></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">Date</span>
        <ol class="breadcrumb">
            <li><a href=""><?=date('M d, Y')?></a></li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">

                    <h5 class="subtitle mb10">From</h5>
                    <img src="" class="img-responsive mb10" alt="" />
                    <address>
                        <strong>Eshop E-commerce Ltd</strong><br>
                        Motijheel, Dhaka-1209<br>
                        <abbr title="Phone">Phone:</abbr> +88-01556984227
                    </address>

                </div><!-- col-sm-6 -->

                <div class="col-sm-6 text-right">
                    <h5 class="subtitle mb10">Invoice No: <?=$order->invoice_id?></h5>
                    <h4 class="text-primary">Order No: <?=$order->order_id?></h4>

                    <h5 class="subtitle mb10">To</h5>
                    <address>
                        <strong><?=$order->f_name.' '.$order->l_name?></strong><br>
                        <?=$order->address_1?><br>
                        <?=$order->district_name?><br>
                        <abbr title="Phone">Mobile: </abbr><?=$order->mobile_phone?><br>
                        <abbr title="Phone">Email: </abbr><?=$order->email?>
                    </address>

                    <p><strong>Invoice Date: </strong><?php
                        $date=strtotime($order->created_at);
                        echo date('M d, Y',$date);
                        ?></p>

                </div>
            </div><!-- row -->

            <div class="table-responsive">
                <table class="table table-invoice table-bordered">
                    <thead>
                    <tr>
                        <th style="">SN</th>
                        <th style="width:50%; text-align: left">Item</th>
                        <th style="">Quantity</th>
                        <th style="width: 15%">Unit Price</th>
                        <th style="width: 15%">Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=0; foreach($product as $p){$i+=1;?>
                    <tr>
                        <td><?=$i?></td>
                        <td style="text-align: left">
                            <div class="text-primary"><strong><?=$p->product_name?></strong></div>
                            <small></small>
                        </td>
                        <td><?=$p->qty?></td>
                        <td style="text-align: right"><?=$p->price?> /=</td>
                        <td><?=$p->subtotal?> /=</td>
                    </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            <table class="table table-total">
                <tbody>
                <tr>
                    <td><strong>Sub Total :</strong></td>
                    <td><?=$total->subtotal?> /=</td>
                </tr>
                <tr>
                    <td><strong>Delivery Charge :</strong></td>
                    <td>50 /=</td>
                </tr>
                <tr>
                    <td><strong>TOTAL :</strong></td>
                    <td><?=$total->subtotal+50?> /=</td>
                </tr>
                </tbody>
            </table>

            <div class="text-right btn-invoice">
                <button onclick="window.print();" class="btn btn-white">Print Copy</button>
            </div>

            <div class="mb40"></div>

            <div class="well">
                Thank you for being a part of our family. <strong>Eshop E-commerce Ltd</strong>
            </div>


        </div><!-- panel-body -->
    </div><!-- panel -->

</div>
</body>
</html>