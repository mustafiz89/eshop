
<div class="pageheader">
    <h2><i class="fa fa-home"></i><?=$header?></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?=$header?></a></li>
            <li class="active"><?=$active?></li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a href="#" class="panel-close">×</a>
                        <a href="#" class="minimize">−</a>
                    </div><!-- panel-btns -->
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <?php $this->load->view('admin/layout/notification')?>
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered" id="main-category">
                                    <thead>
                                    <tr>
                                        <th width="8%">SN</th>
                                        <th>Order info</th>
                                        <th style="width: 30%">Customer</th>
                                        <th style="width: 10%" >Total</th>
                                        <th style="width: 10%">Status</th>
                                        <th width="">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; foreach($order as $o){ $i+=1?>
                                        <tr>
                                            <td><?=$i?></td>
                                            <td>Order No: <?=$o->order_id?><br>
                                                Invoice No: <?=$o->invoice_id?><br>
                                                Order Date: <?php
                                                $date=strtotime($o->created_at);
                                                echo date('d M Y',$date);
                                                echo '<br>Time: '.date('h:i:s A',$date)
                                                ?>
                                            </td>
                                            <td><?=$o->f_name.' '.$o->l_name?><br>Address:
                                                <?=$o->address_1?><br> District: <?=$o->district_name?><br>
                                                Mobile: <?=$o->mobile_phone?>
                                            </td>
                                            <td><?=$o->total?> ৳</td>
                                            <td>
                                                <?php
                                                if($o->confirmed_status==1){
                                                    echo '<span style="font-size: 14px" class="label label-success">Order confirmed</span><br><br>';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                        Action
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a class="print" href="#" data-id="<?=$o->order_table_id?>"><i class="fa fa-print"></i> Print Preview</a>
                                                            <a class="details" data-id="<?=$o->order_table_id?>"><i class="fa fa-search"></i> View Details</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div><!-- table-responsive -->
                        </div><!-- row -->
                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- col-sm-9 -->
        </div><!-- row -->
    </div>
    <!-- contentpanel -->

    <!--Bootstrap Modal -->
    <div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Order Details</h4>
                </div>
                <div id="show_table" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <script>
        $('#main-category').dataTable({
            "sPaginationType": "full_numbers"
        });

        $('.print').on('click',function(){
            var id=$(this).attr('data-id');
            window.open("<?=base_url()?>shop/order/print_order/"+id, "", "width=800,height=550");

        });

        $('.details').on('click',function(){
            var id=$(this).attr('data-id');
            var csrf='<?=$this->security->get_csrf_hash()?>';
            $.ajax({
                url:'<?=base_url()?>shop/order/order_details',
                type:'post',
                data:{id:id,csrf:csrf},
//            dataType:'json',
                success:function(response){
                    $('#show_table').html(response);
                    $('#modal').modal('show');
                },
                error:function(XHR,txtStatus,errorThrown){
                    console.log('Error: '+errorThrown);
                }
            });
        });

        $('.delete').on('click',function(){
            var check=confirm("Want to delete it?");
            if(check){
                return true;
            }
            else{
                return false;
            }
        });

    </script>