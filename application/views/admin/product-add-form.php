<div class="pageheader">
    <h2><i class="fa fa-home"></i><?= $header ?></h2>

    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href=""><?= $site ?></a></li>
            <li class="active"><?= $active ?></li>
        </ol>
    </div>
</div>
<div class="contentpanel">

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Selected category for product</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Main Category Name:</label>

                            <div class="col-sm-4">
                                <input id="disabledinput" class="form-control" value="<?=$main_category_name?>" disabled="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Category Name:</label>

                            <div class="col-sm-4">
                                <input id="disabledinput" class="form-control" value="<?=$category_name?>" disabled="" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Sub Category</label>

                            <div class="col-sm-4">
                                <input id="disabledinput" class="form-control" value="<?=$sub_category_name?>" disabled="" type="text">
                            </div>
                        </div>
                        <div class="form-group">

                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
    <!-- row -->
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <form class="form-horizontal form-bordered" action="<?=base_url()?>shop/product/save_product" method="post" enctype="multipart/form-data">
                <input id="csrf" type="hidden" name="<?=$this->security->get_csrf_token_name()?>"
                       value="<?=$this->security->get_csrf_hash()?>"/>
                <input type="hidden"  class="form-control" value="<?=$main_category_id?>"  name="main_category_id">
                <input type="hidden"  class="form-control" value="<?=$category_id?>"  name="category_id">
                <input type="hidden"  class="form-control" value="<?=$sub_category_id?>"  name="sub_category_id">


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="#" class="panel-close">&times;</a>
                            <a href="#" class="minimize">&minus;</a>
                        </div>
                        <h4 class="panel-title">Edit product</h4>
                    </div>
                    <div class="panel-body panel-body-nopadding">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $this->load->view('admin/layout/notification') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Title</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Product Title" name="product_title" required value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Price</label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" placeholder="0.0" name="product_price" required>
                            </div>
                            <div class="col-sm-1">
                                <p style="font-size: 30px;padding-top:10px">৳</p>
                            </div>
                            <div class="col-sm-2">
                                <div class="ckbox ckbox-primary">
                                    <input value="" id="checkboxPrimary" type="checkbox">
                                    <label for="checkboxPrimary">Previous Price</label>
                                </div>
                            </div>
                        </div>
                        <div id="previous_price" class="form-group">
                            <label style="color:#008855" class="col-sm-3 control-label">Previous Price</label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" placeholder="0.0" name="product_previous_price">
                            </div>
                            <div class="col-sm-1">
                                <p style="font-size: 30px;padding-top:10px">৳</p>
                            </div>
                        </div>

                        <div id="" class="form-group discount">
                            <label class="col-sm-3 control-label">Discount</label>
                            <div class="col-sm-2">
                                <input class="form-control" type="text" placeholder="0.0" name="discount">
                            </div>
                            <div class="col-sm-1">
                              <p style="font-size: 30px;padding-top:10px">%</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Avialibility</label>
                            <div class="col-sm-2">
                                <select id="" name="stock_status" class="form-control input-md"
                                        required>
<!--                                    <option value="">--SELECT--</option>-->
                                    <option value="1">In Stock</option>
                                    <option value="2">Out of Stock</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Condition</label>
                            <div class="col-sm-2">
                                <select id="" name="product_condition" class="form-control input-md"
                                        required>
<!--                                    <option value="">--SELECT--</option>-->
                                    <option value="1">New</option>
                                    <option value="2">Brand New</option>
                                    <option value="4">Used</option>
                                    <option value="3">Old</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Status</label>
                            <div class="col-sm-2">
                                <select id="" name="product_status" class="form-control input-md"
                                        required>
                                    <option value="1">SHOW</option>
                                    <option value="0">HIDDEN</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Product Default image</label>
                            <div class="col-sm-4">
                               <input type="file" name="product_default_image" accept="image/*" required>
                                <p>Image size should be 850*1036</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Color</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="color" placeholder="Color" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Size</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="size" placeholder="e.g  s, m, l, xl, xll etc">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Waist</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="waist" placeholder="e.g  28, 29, 30 etc">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Features</label>
                            <div class="col-sm-8">
                                <textarea name="product_details" id="wysiwyg" placeholder="Enter Features here..." class="form-control" rows="15"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Technical Specification</label>
                            <div class="col-sm-8">
                                <textarea name="technical" id="technical" placeholder="Enter Technical specification here..." class="form-control" rows="15"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Support</label>
                            <div class="col-sm-8">
                                <textarea name="support" id="support" placeholder="Enter support details here..." class="form-control" rows="15"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Manual</label>
                            <div class="col-sm-8">
                                <textarea name="manual" id="manual" placeholder="Enter Manual details here..." class="form-control" rows="15"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-primary">Save Product in those category</button>
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel-default -->
            </form>
        </div>
        <!-- col-sm-9 -->
    </div>
</div>
<script>
    $(function(){
        $('#wysiwyg').wysihtml5({color: true,html:true});
        $('#support').wysihtml5({color: true,html:true});
        $('#technical').wysihtml5({color: true,html:true});
        $('#manual').wysihtml5({color: true,html:true});
        $('#previous_price').hide();
        $('.discount').show();
        $('#checkboxPrimary').on('change',function(){
            var val=$(this).attr('checked');
            if(val!=='checked'){
                $('#previous_price').hide();
                $('.discount').show();
            }
            else{
                $('#previous_price').show();
                $('.discount').hide();

            }
        });

    });

</script>