<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->user_id = (int)$this->session->userdata('user_id');

        if ($this->user_id <= 0) {
            redirect('auth/login', 'refresh');
        }
    }

    public function view($address,$data){
        $data['title'] = "";
        $data['content'] = $this->load->view($address, $data, true);
        $this->load->view('admin/admin-layout', $data);
    }

    public function index(){
        $user=new User_model();
        $data['users']=$user->getAllUser();
        $this->view('admin/user',$data);
    }

    public function save_user(){
        if($this->form_validation->run('user')!==false){
            $user=new User_model();
            $image=$user->saveUserImage('user','user');
            $params=$this->input->post(array("full_name","username","email","role"));
            $params['password']=md5($this->input->post('password'));
            $params['create_at']=date('Y-m-d');
            $params['image']=$image;
            $user->saveUser($params);
            $this->session->set_userdata('success','Save successfully');
            redirect('dashboard/user');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('dashboard/user');
        }
    }

    public function select_user(){
        $user_id=$this->input->post('user_id');
        $model=new User_model();
        $data=$model->getUser($user_id,"user_id");
        echo json_encode($data);
    }

    public function update_user(){
        if($this->form_validation->run('user')!==false) {
            $user=new User_model();
            $id=$this->input->post('user_id');
            $image=$user->updateUserImage('user',$id);
            $params=$this->input->post(array("full_name","username","email","role"));
            $params['password']=md5($this->input->post('password'));
            $params['update_at']=date('Y-m-d');
            if($image!=""){
                $params['image']=$image;
            }
//            echo "<pre>";
//            print_r($params);
//            exit;
            $user->updateUser($id,$params);
            $this->session->set_userdata('info','User update successfully');
            redirect('dashboard/user');

        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('dashboard/user');
        }
    }

    public function delete_user($id){
        $user=new User_model();
        $path = $user->getUser($id,'user_id');
        if ($path->image) {
            $image = $path->image;
            unlink($image);
        }
        $user->deleteUser($id);
        $this->session->set_userdata('error','Delete successfully');
        redirect('dashboard/user');
    }

    public function edit_user_profile($id){

        $model=new User_model();
        $data['user']=$model->getUser($id,"user_id");
        $this->view('admin/user-profile',$data);
    }

    public function update_user_profile(){
        $id=$this->input->post('user_id');
        if($this->form_validation->run('user')!==false) {
            $user=new User_model();
            $image=$user->updateUserImage('user',$id);
            $params=$this->input->post(array("full_name","username","email","role"));
            $params['password']=md5($this->input->post('password'));
            $params['update_at']=date('Y-m-d');
            if($image!=""){
                $params['image']=$image;
            }
//            echo "<pre>";
//            print_r($params);
//            exit;
            $user->updateUser($id,$params);
            $this->session->set_userdata('info','User update successfully');
            redirect('dashboard/user/edit_user_profile/'.$id);

        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('dashboard/user/edit_user_profile/'.$id);
        }
    }


}