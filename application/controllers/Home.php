<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
//		$this->user_id = (int)$this->session->userdata('user_id');
//		if ($this->user_id <= 0) {
//			redirect('auth/login', 'refresh');
//		}
		date_default_timezone_set('Asia/Dhaka');
	}

	private function view($address, $data)
	{
		$category=new Category_model();
		$data['main_category']=$category->selectAllActiveMainCategory();
		$data['content'] = $this->load->view($address, $data, true);
		$this->load->view('shop/site-layout',$data);
	}

	public function index()
	{
		$data['body_class']="home";
		$category=new Category_model();
		$data['all_widget_category']=$category->selectAllActiveCategoryByWidget();
		$this->view('shop/home',$data);
	}

	public function category(){
		$data['body_class']="category-page right-sidebar";
		$this->view('shop/category',$data);
	}

	/*
	 * SElect Product For Details By Product Id
	 * Show in site->Product Details page
	 */
	public function product_details($product_id){
		$data['body_class']="";
		$product=new Product_model();
		$s=$product->selectSubCategoryIdByProductId($product_id);
		$data['related_product']=$product->selectAllProductBySubCategoryId($s->sub_category_id,$product_id);
		$data['product']=$product->selectProductByProductId($product_id);
		$this->view('shop/product_details',$data);
	}

	public function login(){
		$data['body_class']="";
		$this->view('shop/login',$data);
	}

	public function wishlist(){
		$data['body_class']="";
		$this->view('shop/wishlist',$data);
	}

	public function checkout(){
		$data['body_class']="";
		$this->view('shop/checkout',$data);
	}
	public function compare(){
		$data['body_class']="";
		$this->view('shop/compare',$data);
	}

	/*
	 * Select all product By Sub category ID
	 * show in site->all category page
	 */
	public function product_by_sub_category($sub_category_id){
		$data['body_class']="category-page right-sidebar";
		$product=new Product_model();
		
		//		For Right side subcategory Bar
		$category=new Category_model();
		$category_id=$category->selectCategoryIdBySubCategoryId($sub_category_id);
		$data['category_id']=$category_id->category_id;
		//		For Right side subcategory Bar
		
		$this->load->library('pagination');
		$config['base_url'] = base_url() .'home/product_by_sub_category/'.$sub_category_id;
		$config['total_rows'] = $this->db->where('sub_category_id',$sub_category_id)->get('tbl_product')->num_rows();
		$config['per_page'] = '18';
		$config['cur_tag_open'] = '<a><b>';
		$config['cur_tag_close'] = '</b></a>';
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next ';
		$this->pagination->initialize($config);
		$data['all_product']=$product->selectAllActiveProductBySubcategoryId($sub_category_id,$config['per_page'], $this->uri->segment(4));
		$this->view('shop/category',$data);
	}

	/*
	 * Select all product By Category ID
	 * Show in site->all category page
	 */

	public function product_by_category($category_id){
		$data['body_class']="category-page right-sidebar";
		$data['category_id']=$category_id;
		$product=new Product_model();
		$this->load->library('pagination');
		$config['base_url'] = base_url() .'home/product_by_category/'.$category_id;
		$config['total_rows'] = $this->db->where('sub_category_id',$category_id)->get('tbl_product')->num_rows();
		$config['per_page'] = '18';
		$config['cur_tag_open'] = '<a><b>';
		$config['cur_tag_close'] = '</b></a>';
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next ';
		$this->pagination->initialize($config);
		$data['all_product']=$product->selectAllActiveProductByCategoryId($category_id,$config['per_page'], $this->uri->segment(4));
		$this->view('shop/category',$data);
	}



//    Manipulating CART Product

	public function add_product_to_cart(){
		$id=$this->input->post('product_id');
		$qty=$this->input->post('qty');
		$model=new Product_model();
		$product=$model->selectProductByProductId($id);
		$data=array(
			'id'=>$product->product_id,
			'qty'=>$qty,
			'price'=>$product->product_price,
			'name'=>$product->product_title,
			'option'=>array(
				'img'=>$product->product_default_image,
				'stock'=>$product->stock_status,
				'size'=>'',
				'color'=>'',
				'waist'=>''
			)
		);

		$row_id=$this->cart->insert($data);
		$items['cart_item']=$this->cart->get_item($row_id);
//		$this->cart->destroy();
		$items['items']=$this->cart->total_items();
		$items['total']=$this->cart->total();
		echo json_encode($items);
	}

	public function show_cart(){
		$this->load->view('shop/layout/cart-view');
	}

	public function order(){
		$model=new Common_model();
		$data['body_class']="";
		$items=$this->cart->total_items();
		$step=$this->input->get('step');
		if($items>0){
			if($step==0||$step=='') {
				$data['active']='';
				$data['cart_content'] = $this->load->view('shop/order/cart-summary', $data, true);
				$data['summary']='current-step';
				$this->view('shop/order', $data);
			}
			else if($step==1){
				$data['summary']='current-step';
				$data['signin']='current-step';
				if($this->session->userdata('customer_id')){
					redirect('home/order?step=2');
				}
				else{
					$data['cart_content']=$this->load->view('shop/order/customer-login',$data,true);
					$this->view('shop/order',$data);
				}
			}
			else if($step==2){
				$data['summary']='current-step';
				$data['signin']='current-step';
				$data['address']='current-step';
				if($customer_id=$this->session->userdata('customer_id')){
					$data['customer']=$model->selectRow('tbl_customer',$customer_id,'customer_id');
					$data['district']=$model->selectAllRow('tbl_district');
					$data['cart_content']=$this->load->view('shop/order/customer-address',$data,true);
					$this->view('shop/order',$data);

				}
				else{
					redirect('home/order?step=1');
				}
			}
			else if($step==3){
				$data['summary']='current-step';
				$data['signin']='current-step';
				$data['address']='current-step';
				$data['payment']='current-step';
				if($customer_id=$this->session->userdata('customer_id')){
					$data['customer']=$model->selectCustomerWithDistrict($customer_id);
					$data['cart_content']=$this->load->view('shop/order/payment',$data,true);
					$this->view('shop/order',$data);

				}
				else{
					redirect('home/order?step=1');
				}
			}
		}
		else{
			$this->view('shop/order',$data);
		}
	}

	public function login_customer_from_order(){
		$model=new Common_model();
		$params=array(
			'email'=>$this->input->post('email_2',true),
			'password'=>md5($this->input->post('password_2',true))
		);

		if($this->form_validation->run('customer_login')!==false) {
			$result=$model->check_customer_credentials($params);
			if($result){
				$sdata=array(
					'f_name'=>$result->f_name,
					'l_name'=>$result->l_name,
					'customer_id'=>$result->customer_id
				);
				$this->session->set_userdata($sdata);
				redirect('home/order?step=2');
			}
			else{
				$this->session->set_userdata('login_error','Ops! email or Password incorrect. Try again');
				redirect('home/order?step=1');
			}
		}
		else{
			$error=$this->form_validation->error_array();
			$this->session->set_flashdata($error);
			redirect('home/order?step=1');
		}
	}

	public function update_address_from_order(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		$params=$this->input->post(array('mobile_phone','home_phone','address_1','address_2','district_id','additional_information'));
		$params['updated_at']=date('Y-m-d H:i:s',time());
		if($this->form_validation->run('update_customer_address')!==false){
			$model->update('tbl_customer',$params,$customer_id,'customer_id');
			redirect('home/order?step=3');
		}
		else{
			$this->session->set_userdata('error',validation_errors());
			redirect('home/order?step=2');
		}
	}

	public function confirm_order_for_delivery(){
		$params=array();
		$model=new Common_model();
		$params=$this->input->post(array('payment_method','additional_message'));
		$params['customer_id']=$this->session->userdata('customer_id');
		$last_id=$model->get_mysql_last_id('tbl_order','order_table_id');
		if($last_id){
			$invoice_temp=$last_id->order_table_id+1;
		}
		else{
			$invoice_temp=1;
		}
		$params['invoice_id']='INV-ESHOP-'.$invoice_temp;
		$params['order_id']='ORD-'.date('ymd').time();
		$params['total']=$this->cart->total();
		$params['created_at']=date('Y-m-d H:i:s',time());
		$model->create('tbl_order',$params);
		$order_table_id=$this->db->insert_id();
//		echo '<pre>';
//		print_r($this->cart->contents());
//		exit;

		foreach($this->cart->contents() as $item){
			$cdata['product_id']=$item['id'];
			$cdata['product_name']=$item['name'];
			$cdata['qty']=$item['qty'];
			$cdata['price']=$item['price'];
			$cdata['subtotal']=$item['subtotal'];
			$cdata['order_table_id']=$order_table_id;
			$cdata['size']=$item['option']['size'];
			$cdata['color']=$item['option']['color'];
			$cdata['waist']=$item['option']['waist'];
			$cdata['created_at']=$params['created_at'];
			$model->create('tbl_order_details',$cdata);
		}
		$this->cart->destroy();
		$this->session->set_userdata('success','Order confirmation successful');
		redirect('home/customer_order_history');

	}

	public function update_quantity(){
		$product_id=$this->input->post('product_id');
		$qty =$this->input->post('qty');
		$params=array(

			'rowid'=>md5($product_id),
			'qty'=>$qty,
		);
		$this->cart->update($params);
		$data['items']=$this->cart->total_items();
		$data['product']=$this->cart->get_item(md5($product_id));
		$data['total']=$this->cart->total();

		echo json_encode($data);
	}

	public function remove_cart_item(){
		$product_id=$this->input->post('product_id');
		$this->cart->remove(md5($product_id));
		$data['items']=$this->cart->total_items();
		$data['total']=$this->cart->total();
		$data['cart_summary']=$this->load->view('shop/order/cart-summary','',true);
		echo json_encode($data);
	}

	/*
	 * Client/Customer Registration
	 */

	public function registration_form(){
		$data['body_class']="";
		$this->view('shop/customer/customer-registration',$data);
	}

	public function check_email(){
		$response=array();
		$model=new Common_model();
		$email=$this->input->post('email');
		$check_mail=$model->selectRow('tbl_customer',$email,'email');
		if($check_mail){
			$response['status']='error';
			$response['message']='Already registered.Please try another';
		}
		else{
			$response['status']='success';
			$response['message']='Available';
		}
		echo json_encode($response);
	}

	public function save_customer(){
		$model=new Common_model();
		if($this->form_validation->run('customer')!==false){
			$params=$this->input->post(array('f_name','l_name','email','gender'));
			$params['password']=md5($this->input->post('password'));
			$params['created_at']=date('Y-m-d H:i:s',time());
			$check_email=$model->selectRow('tbl_customer',$params['email'],'email');
			if($check_email){
				$this->session->set_userdata('error','This email has been already registered! Please use another.');
				redirect('home/registration_form');
			}
			else{
				$model->create('tbl_customer',$params);
				$mdata['name']=$params['f_name'].' '.$params['l_name'];
				$mdata['email']=$params['email'];
				$mdata['password']=$this->input->post('password');
				$body=$this->load->view('mail/customer-registration',$mdata,true);
				$subject="Customer registration email";
				$receiver=$params['email'];
				$model->sendMail($subject,$receiver,$body);
				$this->session->set_userdata('success','User registration has been successful. Now you can login to your account.');
				redirect('home/registration_form');
			}

		}
		else{
			$error=$this->form_validation->error_array();
			$this->session->set_flashdata($error);
			redirect('home/registration_form');
		}
	}

	public function login_customer(){
		$model=new Common_model();
		$params=array(
			'email'=>$this->input->post('email_2',true),
			'password'=>md5($this->input->post('password_2',true))
		);

		if($this->form_validation->run('customer_login')!==false) {
			$result=$model->check_customer_credentials($params);
			if($result){
				$sdata=array(
					'f_name'=>$result->f_name,
					'l_name'=>$result->l_name,
					'customer_id'=>$result->customer_id
				);
				$this->session->set_userdata($sdata);
				redirect('home');
			}
			else{
				$this->session->set_userdata('login_error','Ops! email or Password incorrect. Try again');
				redirect('home/registration_form');
			}
		}
		else{
			$error=$this->form_validation->error_array();
			$this->session->set_flashdata($error);
			redirect('home/registration_form');
		}
	}

	public function customer_profile(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		if($customer_id){
//			$data['customer']=$model->selectRow('tbl_customer',$customer_id,'customer_id');
			$data['body_class']="";
			$this->view('shop/customer/customer-profile',$data);
		}
		else{
			redirect('home');
		}
	}

	public function show_personal_info(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		if($customer_id){
			$data['customer']=$model->selectRow('tbl_customer',$customer_id,'customer_id');
			$data['body_class']="";
			$this->view('shop/customer/customer-personal-info',$data);
		}
		else{
			redirect('home');
		}
	}
	public function update_personal_info(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		$params=$this->input->post(array('f_name','l_name','email','gender'));
        $current_password=$this->input->post('old_password');
		$params['password']=md5($this->input->post('password'));
		$params['updated_at']=date('Y-m-d H:i:s',time());
		if($this->form_validation->run('update_customer')!==false){
			$check_password=$model->selectRow('tbl_customer',$customer_id,'customer_id');
			if($check_password->password==md5($current_password)){
				$model->update('tbl_customer',$params,$customer_id,'customer_id');
				$this->session->set_userdata('success','Profile update successfully');
            }
            else{
                $this->session->set_userdata('error',"Your current password isn't match. Please Try again");
            }
		}
        else{
            $this->session->set_userdata('error',validation_errors());
        }
		redirect('home/show_personal_info');
	}

	public function show_customer_address(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		if($customer_id){
			$data['customer']=$model->selectRow('tbl_customer',$customer_id,'customer_id');
			$data['district']=$model->selectAllRow('tbl_district');
			$data['body_class']="";
			$this->view('shop/customer/customer-address',$data);
		}
		else{
			redirect('home');
		}
	}

	public function update_customer_address(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		$params=$this->input->post(array('mobile_phone','home_phone','address_1','address_2','district_id','additional_information'));
		$params['updated_at']=date('Y-m-d H:i:s',time());
		if($this->form_validation->run('update_customer_address')!==false){
				$model->update('tbl_customer',$params,$customer_id,'customer_id');
				$this->session->set_userdata('success','Address update successfully');
		}
		else{
			$this->session->set_userdata('error',validation_errors());
		}
		redirect('home/show_customer_address');
	}


	public function customer_order_history(){
		$model=new Common_model();
		$customer_id=$this->session->userdata('customer_id');
		if($customer_id){
			$data['order']=$model->order_list_by_date($customer_id);
			$data['body_class']="";
			$this->view('shop/customer/customer-order-history',$data);
		}
		else{
			redirect('home');
		}
	}

	public function view_details_order(){
		$model=new Common_model();
		$order_table_id=$this->input->post('id');
		$data['order']=$model->get_order_by_order_id($order_table_id);
		$data['product']=$model->get_order_list_by_order_id($order_table_id);
		$data['total']=$model->total_price($order_table_id);
		$this->load->view('shop/customer/customer-order-details',$data);
	}

	public function print_order($order_table_id){
		$model=new Common_model();
		$data['order']=$model->get_order_by_order_id($order_table_id);
		$data['product']=$model->get_order_list_by_order_id($order_table_id);
		$data['total']=$model->total_price($order_table_id);
		$this->load->view('shop/order/print-order',$data);
	}

	public function logout_customer(){
		$this->session->unset_userdata('customer_id');
		$this->session->unset_userdata('f_name');
		$this->session->unset_userdata('l_name');
		redirect('home');
	}

}
