<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    private $user_id ;

    public function __construct()
    {
        parent::__construct();

        $this->user_id = (int)$this->session->userdata('user_id');

        if($this->user_id > 0)
        {
            redirect('auth/admin', 'refresh');
        }
    }

    public function index()
    {
        $this->load->view('admin/layout/login-layout');
    }

    public function check_credential(){
        $params=array(
            'username'=>$this->input->post('username',true),
            'password'=>md5($this->input->post('password',true))
        );

        if($this->form_validation->run('login')!==false){
            $result=$this->User_model->checkCredential($params);
            if($result){
                $sdata['user_id']=$result->user_id;
                $sdata['full_name']=$result->full_name;
                $sdata['role']=$result->role;
                $sdata['image']=$result->image;
//                echo'<pre>';
//                print_r($sdata);
//                exit;
                $this->session->set_userdata($sdata);
                redirect('auth/admin');
            }
            else{
                $this->session->set_userdata('error','Username or Password incorrect!');
                redirect('auth/login');
            }
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('auth/login');
        }
    }
}
