<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        $this->user_id = (int)$this->session->userdata('user_id');

        if ($this->user_id <= 0) {
            redirect('auth/login', 'refresh');
        }
    }

    public function index(){
        $data['content']=$this->load->view('admin/dashboard','',true);
        $this->load->view("admin/admin-layout",$data);
    }

    public function logout(){
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_image');
        $this->session->set_flashdata('info','Successfully logout');
        redirect('auth/login');
    }
}
