<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->user_id = (int)$this->session->userdata('user_id');
        if ($this->user_id <= 0) {
            redirect('auth/login', 'refresh');
        }
        date_default_timezone_set('Asia/Dhaka');
    }

    private function view($address, $data){
        $data['title'] = "";
        $data['site'] = "Eshop";
        $data['content'] = $this->load->view($address, $data, true);
        $this->load->view('admin/admin-layout', $data);
    }

    public function index(){
        redirect('auth/admin');
    }

    /*
     * Menu Category function
     */
    public function manage_main_category(){
        $data['header']="Menu Category";
        $data['active']="Menu Category";
        $model=new Common_model();
        $data['all_category']=$model->selectAllRow('tbl_main_category');
        $this->view('admin/main-category',$data);
    }

    public function save_main_category(){
        if($this->form_validation->run('main_category')!==false){
            $insert=new Common_model();
            $params=$this->input->post(array('main_category_name','main_category_status'),true);
            date_default_timezone_set('Asia/Dhaka');
            $params['created_at']=date('Y-m-d H:i:s',time());
//            print_r($params);
//            exit;
            $insert->create('tbl_main_category',$params);
            $this->session->set_userdata('success','Save successfully');
            redirect('shop/category/manage_main_category');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/manage_main_category');
        }
    }

    public function select_main_category_by_id(){
        $category_id=$this->input->post('category_id');
        $model=new Common_model();
        $data=$model->selectRow('tbl_main_category',$category_id,'main_category_id');
        echo json_encode($data);
    }

    public function update_main_category(){
        if($this->form_validation->run('main_category')!==false){
            $insert=new Common_model();
            $category_id=$this->input->post('main_category_id');
            $params=$this->input->post(array('main_category_name','main_category_status'),true);
//            print_r($params);
//            exit;
            $insert->update('tbl_main_category',$params,$category_id,'main_category_id');
            $this->session->set_userdata('success','Update successfully');
            redirect('shop/category/manage_main_category');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/manage_main_category');
        }
    }

    public function delete_main_category($id){
        $model=new Common_model();
        $model->delete('tbl_main_category',$id,'main_category_id');
        $this->session->set_userdata('error','Delete successfully');
        redirect('shop/category/manage_main_category');
    }

    /*
     * Menu Category function
     */

    /*
     * Category Function
     */

    public function manage_category(){
        $data['header']="Category";
        $data['active']="Category";
        $category=new Category_model();//Initialize category model in object
        $data['main_category']=$category->selectAllActiveMainCategory();
        $data['category']=$category->selectAllCategory();
        $this->view('admin/category',$data);
    }

    public function save_category(){
        if($this->form_validation->run('category')!==false){
            $insert=new Common_model();
            $params=$this->input->post(array('category_name','main_category_id','category_status'),true);
            date_default_timezone_set('Asia/Dhaka');
            $params['created_at']=date('Y-m-d H:i:s',time());
//            print_r($params);
//            exit;
            $insert->create('tbl_category',$params);
            $this->session->set_userdata('success','Save successfully');
            redirect('shop/category/manage_category');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/manage_category');
        }
    }

    public function select_category_by_id(){
        $category_id=$this->input->post('category_id');
        $model=new Common_model();
        $data=$model->selectRow('tbl_category',$category_id,'category_id');
        echo json_encode($data);
    }

    public function update_category(){
        if($this->form_validation->run('category')!==false){
            $insert=new Common_model();
            $category_id=$this->input->post('category_id');
            $params=$this->input->post(array('category_name','main_category_id','category_status'),true);
//            print_r($params);
//            exit;
            $insert->update('tbl_category',$params,$category_id,'category_id');
            $this->session->set_userdata('success','Update successfully');
            redirect('shop/category/manage_category');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/manage_category');
        }

    }

    public function delete_category($id){
        $model=new Common_model();
        $model->delete('tbl_category',$id,'category_id');
        $this->session->set_userdata('error','Delete successfully');
        redirect('shop/category/manage_category');
    }

    public function featured_category($category_id){
        $data['header']="Featured Category";
        $data['active']="Featured Category";
        $data['category_id']=$category_id;
        $category=new Category_model();
        $data['widget_image']=$category->selectAllCategoryImageById($category_id);
        $data['category']=$category->selectCategoryById($category_id);
        $this->view('admin/category-featured',$data);
    }

    public function upload_featured_category_image(){
        $category_id=$this->input->post('category_id');
        $data=array();
        $config['upload_path'] = 'public/upload/category/'; //give the path to upload the image in folder
        $config['remove_spaces']=TRUE;
        $config['max_width'] = 386;
        $config['max_height'] = 500;
        $config['allowed_types'] = 'gif|jpg|jpeg|JPG|png';
        $config['max_size']	= '5000';


        $files = $_FILES;
        $count = count($_FILES['widget_image']['name']);
        for($i=0; $i<$count; $i++)
        {
            $_FILES['widget_image']['name']= $files['widget_image']['name'][$i];
            $_FILES['widget_image']['type']= $files['widget_image']['type'][$i];
            $_FILES['widget_image']['tmp_name']= $files['widget_image']['tmp_name'][$i];
            $_FILES['widget_image']['error']= $files['widget_image']['error'][$i];
            $_FILES['widget_image']['size']= $files['widget_image']['size'][$i];
            $new_name = date('y-m-d')."_".time().'_'.$i.'_'.$category_id;
            $config['file_name'] = $new_name;
            $this->load->library('upload',$config);
            $this->load->library('image_lib');
//            $this->upload->do_upload('widget_image');
            if (!$this->upload->do_upload('widget_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('info', $error);
                redirect('shop/category/featured_category/'.$category_id);

            } else {
                $upload_data = $this->upload->data();
                $data[$i]['widget_image'] = $config['upload_path'] . $upload_data['file_name'];
                $data[$i]['category_id'] = $category_id;
//                $resize_conf = array(
//                    'source_image' => $upload_data['full_path'],
//                    'new_image' => $upload_data['file_path'],
//                    'width' => 400,
//                    'height' => 600,
////                    'maintain_ratio' => true,
////                    'overwrite' => true
//                );
//
//                $this->image_lib->initialize($resize_conf);
//                $this->image_lib->resize();
            }

        }

        $model=new Common_model();
        $image=$model->insert_batch('tbl_category_image',$data);
        if($image){
            $this->session->set_userdata('info','Image uploaded successfully');
        }
        redirect('shop/category/featured_category/'.$category_id);
    }
    public function save_featured_category(){
        $model=new Common_model();
        $category_id=$this->input->post('category_id',true);
        $params=$this->input->post(array('widget_title','widget_color'),true);
        $params['widget_status']=1;
        $model->update('tbl_category',$params,$category_id,'category_id');
        $this->session->set_userdata('success','Save featured option successfully');
        redirect('shop/category/featured_category/'.$category_id);
    }

    public function hide_featured_category($id){
        $this->db->set('widget_status',0)->where('category_id',$id)->update('tbl_category');
        $this->session->set_userdata('success','Hide option successfully');
        redirect('shop/category/manage_category');
    }

    public function delete_featured_image($id,$category_id){
        $model=new Common_model();
        $path=$model->selectRow('tbl_category_image',$id,'category_image_id');
        unlink($path->widget_image);
        $model->delete('tbl_category_image',$id,'category_image_id');
        $this->session->set_userdata('info','Image delete successfully');
        redirect('shop/category/featured_category/'.$category_id);
    }
    /*
     * Category function
     */
//--------------------------------------------------------------------------
    /*
     * Sub Category Function
     */
    public function sub_category_form(){
        $data['header']="Sub Category";
        $data['active']="Sub category";
        $model=new Category_model();
        $data['main_category']=$model->selectAllActiveMainCategory();
        $this->view('admin/sub-category-form',$data);
    }

    public function save_sub_category(){
        if($this->form_validation->run('sub_category')!==false){
            $insert=new Common_model();
            $params=$this->input->post(array('sub_category_name','main_category_id','category_id','sub_category_status'),true);
            date_default_timezone_set('Asia/Dhaka');
            $params['created_at']=date('Y-m-d H:i:s',time());
//            print_r($params);
//            exit;
            $insert->create('tbl_sub_category',$params);
            $this->session->set_userdata('success','Save successfully');
            redirect('shop/category/sub_category_form');
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/sub_category_form');
        }
    }

    public function select_category_by_main_category_id(){  //Default category get function you can use dropdown selection
        $id=$this->input->post('id',true);
        $model=new Category_model();
        $category=$model->selectCategoryByMainCategoryId($id);
        echo '<option value="">--SELECT--</option>';
        foreach($category as $c){
            echo '<option value="'.$c->category_id.'">'.$c->category_name.'</option>';
        }
    }

//    public function manage_sub_category(){
//        $data['header']="View all sub-category";
//        $data['active']="Sub category";
//        $model=new Category_model();
//        $data['main_category']=$model->selectAllActiveMainCategory();
//        $this->view('admin/view-sub-category',$data);
//    }

    public function manage_sub_category(){
        $main_category_id=$this->input->get('main_category_id');
        $category_id=$this->input->get('category_id');
        $data['header']="View all sub-category";
        $data['active']="Sub category";
        $model=new Category_model();
        $data['main_category']=$model->selectAllActiveMainCategory();
        $data['category']=$model->selectAllActiveCategory();
        $data['sub_category']=$model->selectAllSubcategoryByIds($main_category_id,$category_id);
        $this->session->set_userdata('string',$_SERVER['QUERY_STRING']);
        $this->view('admin/view-sub-category',$data);
    }

    public function select_sub_category_by_id(){
        $sub_category_id=$this->input->post('sub_category_id',true);
        $model=new Category_model();
        echo json_encode($model->selectSubCategoryById($sub_category_id));
    }

    public function update_sub_category(){
        $string=$this->session->userdata('string');
        if($this->form_validation->run('sub_category')!==false){
            $insert=new Common_model();
            $sub_category_id=$this->input->post('sub_category_id',true);
            $params=$this->input->post(array('sub_category_name','category_id','main_category_id','sub_category_status'),true);
//            print_r($params);
//            exit;
            $insert->update('tbl_sub_category',$params,$sub_category_id,'sub_category_id');
            $this->session->set_userdata('success','Update successfully');

//            exit;
            redirect('shop/category/manage_sub_category?'.$string);
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect('shop/category/manage_sub_category?'.$string);
        }

    }

    public function delete_sub_category($id){
        $model=new Common_model();
        $model->delete('tbl_sub_category',$id,'sub_category_id');
        $this->session->set_userdata('error','Delete successfully');
        redirect('shop/category/manage_sub_category');
    }

    public function select_sub_category_by_category_id(){
        $id=$this->input->post('id',true);
        $model=new Category_model();
        $sub_category=$model->selectSubCategoryByCategoryId($id);
        echo '<option value="">--SELECT--</option>';
        foreach($sub_category as $s){
            echo '<option value="'.$s->sub_category_id.'">'.$s->sub_category_name.'</option>';
        }
    }

}