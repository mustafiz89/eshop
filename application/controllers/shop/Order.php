<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->user_id = (int)$this->session->userdata('user_id');
        if ($this->user_id <= 0) {
            redirect('auth/login', 'refresh');
        }
        date_default_timezone_set('Asia/Dhaka');
    }

    private function view($address, $data)
    {
        $data['title'] = "";
        $data['site'] = "Eshop";
        $data['content'] = $this->load->view($address, $data, true);
        $this->load->view('admin/admin-layout', $data);
    }

    public function index()
    {
        redirect('auth/admin');
    }

    public function view_complete_order(){
        $data['header'] = "Order";
        $data['active'] = "Completed";
        $model = new Order_model();
        $data['order'] = $model->get_complete_order();
        $this->view('admin/order/view-complete-order', $data);
    }

    public function view_pending_order(){
        $data['header'] = "Order";
        $data['active'] = "Pending";
        $model = new Order_model();
        $data['pending_order'] = $model->get_pending_order();
        $this->view('admin/order/view-pending-order', $data);
    }

    public function confirm_order($order_table_id){
        $data['confirmed_status']=1;
        $data['confirmed_at']=date('Y-m-d H:i:s');
        $this->db->where('order_table_id',$order_table_id)
            ->update('tbl_order',$data);
        $this->session->set_userdata('success','Order is confirmed');
        redirect('shop/order/view_pending_order');
    }

    public function print_order($order_table_id){
        $model=new Common_model();
        $data['order']=$model->get_order_by_order_id($order_table_id);
        $data['product']=$model->get_order_list_by_order_id($order_table_id);
        $data['total']=$model->total_price($order_table_id);
        $this->load->view('admin/order/print-order',$data);
    }

    public function order_details(){
        $model=new Common_model();
        $order_table_id=$this->input->post('id');
        $data['order']=$model->get_order_by_order_id($order_table_id);
        $data['product']=$model->get_order_list_by_order_id($order_table_id);
        $data['total']=$model->total_price($order_table_id);
        $this->load->view('admin/order/order-details',$data);
    }

}