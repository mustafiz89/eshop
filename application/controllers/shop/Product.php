<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->user_id = (int)$this->session->userdata('user_id');
        if ($this->user_id <= 0) {
            redirect('auth/login', 'refresh');
        }
        date_default_timezone_set('Asia/Dhaka');
    }

    private function view($address, $data)
    {
        $data['title'] = "";
        $data['site'] = "Eshop";
        $data['content'] = $this->load->view($address, $data, true);
        $this->load->view('admin/admin-layout', $data);
    }

    public function index()
    {
        redirect('auth/admin');
    }

    public function select_all_category_for_add_product()
    {
        $data['header'] = "Add Product";
        $data['active'] = "Product";
        $model = new Category_model();
        $data['main_category'] = $model->selectAllActiveMainCategory();
        $this->view('admin/product-category-form', $data);
    }

    public function product_add_form()
    {
        $model = new Category_model();
        $data['header'] = "Add Product";
        $data['active'] = "Product";
        $s_id = $this->input->get('sub_category_id', true);
        $m_id = $this->input->get('main_category_id', true);
        $c_id = $this->input->get('category_id', true);
        $get_data = $model->selectSubCategoryByIds($m_id, $c_id);
        $data['main_category_id'] = $m_id;
        $data['category_id'] = $c_id;
        $data['sub_category_id'] = $s_id;
        $data['main_category_name'] = $get_data->main_category_name;
        $data['category_name'] = $get_data->category_name;
        $data['sub_category_name'] = $get_data->sub_category_name;
        $this->view('admin/product-add-form', $data);
    }

    public function save_product()
    {
        $m_c_id=$this->input->post('main_category_id');
        $c_id=$this->input->post('category_id');
        $s_c_id=$this->input->post('sub_category_id');

        //create a url for redirecting
        $redirect_url = base_url() . 'shop/product/product_add_form?main_category_id='.$m_c_id. '&category_id='.$c_id.
            '&sub_category_id='.$s_c_id;

        //make a image name
        $image_name = $m_c_id . '_' . $c_id . '_' . $s_c_id . '_' . date('Y-m-d') . '_' . time();

        // Check product criteria validation
        if($this->form_validation->run('product')!==false){
            $model = new Product_model();
            $params = $this->input->post(array('main_category_id','category_id','sub_category_id','product_title','color','size','waist',
            'stock_status', 'product_condition', 'product_details','technical','support','manual','product_status'));
            $params['created_at']=date('Y-m-d H:i:s',time());

            $discount=$this->input->post('discount',true);
            $price=$this->input->post('product_price',true);
            $old_price=$this->input->post('product_previous_price',true);
            if($discount>0 &&$old_price>0 ){
                $this->session->set_userdata('error','Please select one between Discount or Previous price');
                redirect($redirect_url);
            }
            else if($discount>0){

                //Calculate product Discount
                $new=floor($price*$discount/100);

                $params['discount']=$discount;
                $params['product_price']=$price-$new;
                $params['product_previous_price']=$price;
            }
            else if($old_price>0){
                $params['product_price']=$price;
                $params['product_previous_price']=$old_price;
                $params['discount']=0;
            }

            else {
                $params['product_price']=$price;
                $params['product_previous_price']='';
                $params['discount']=0;
            }
            $params['product_default_image'] = $model->uploadProductImage($image_name, $redirect_url);
//            echo '<pre>';
//            print_r($params);
//            exit;
            $common=new Common_model();
            $common->create('tbl_product',$params);
            $this->session->set_userdata('success','Product save successfully');
            redirect($redirect_url);
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect($redirect_url);
        }

    }

    public function manage_product()
    {
        $data['header'] = "Manage Product";
        $data['active'] = "Product";
        $model = new Category_model();
        $data['main_category'] = $model->selectAllActiveMainCategory();
        $product=new Product_model();
        $m_id=$this->input->get('main_category_id');
        $c_id=$this->input->get('category_id');
        $s_id=$this->input->get('sub_category_id');
        $redirect_url = base_url() . 'shop/product/manage_product?main_category_id='.$m_id. '&category_id='.$c_id.
            '&sub_category_id='.$s_id;
        $this->session->set_userdata('url',$redirect_url);
        $data['product']=$product->selectAllProductByCategories($m_id,$c_id,$s_id);
        $this->view('admin/manage-product', $data);
    }
    public function select_product_by_product_id(){
        $product=new Product_model();
        $product_id=$this->input->post('product_id');
        echo json_encode($product->selectProductByProductId($product_id));
    }
    public function edit_product($id){
        $data['header'] = "Edit Product";
        $data['active'] = "Product";
        $model=new Common_model();
        $data['product']=$model->selectRow('tbl_product',$id,'product_id');
        $this->view('admin/product-edit-form',$data);

    }

    public function update_product(){
        $m_c_id=$this->input->post('main_category_id');
        $c_id=$this->input->post('category_id');
        $s_c_id=$this->input->post('sub_category_id');
        $product_id=$this->input->post('product_id');

        $redirect_url=base_url().'shop/product/edit_product/'.$product_id;
        //make a image name
        $image_name = $m_c_id . '_' . $c_id . '_' . $s_c_id . '_' . date('Y-m-d') . '_' . time();

        // Check product criteria validation
        if($this->form_validation->run('product')!==false){
            $model = new Product_model();
            $params = $this->input->post(array('main_category_id','category_id','sub_category_id','product_title',
            'stock_status', 'product_condition', 'product_details','technical','support','manual','product_status'));
            

            $discount=$this->input->post('discount',true);
            $price=$this->input->post('product_price',true);
            $old_price=$this->input->post('product_previous_price',true);
            if($discount>0 &&$old_price>0 ){
                $this->session->set_userdata('error','Please select one between Discount or Previous price');
                redirect($redirect_url);
            }
            else if($discount>0){

                //Calculate product Discount
                $new=floor($price*$discount/100);

                $params['discount']=$discount;
                $params['product_price']=$price-$new;
                $params['product_previous_price']=$price;
            }
            else if($old_price>0){
                $params['product_price']=$price;
                $params['product_previous_price']=$old_price;
                $params['discount']=0;
            }

            else {
                $params['product_price']=$price;
                $params['product_previous_price']='';
                $params['discount']=0;
            }
            
            $image=$model->updateProductImage($image_name, $redirect_url,$product_id);
            if($image){
                $params['product_default_image'] = $image;
            }
           
 
        
//            echo '<pre>';
//            print_r($params);
//            exit;
            $common=new Common_model();
            $common->update('tbl_product',$params,$product_id,'product_id');
            $this->session->set_userdata('success','Product update successfully');
            redirect($redirect_url);
        }
        else{
            $this->session->set_userdata('error',validation_errors());
            redirect($redirect_url);
        }
    }

    public function delete_product($id){
        $model=new Common_model();
        $image=$model->selectRow('tbl_product',$id,'product_id');
        unlink('public/upload/product/'.$image->product_default_image);
        unlink('public/upload/product/thumbnail/'.$image->product_default_image);
        $model->delete('tbl_product',$id,'product_id');
        $this->session->set_userdata('info','Product delete successfully');
        redirect($this->session->userdata('url'));
    }
}