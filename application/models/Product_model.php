<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Product_model extends CI_Model
{

    public function uploadProductImage($filename, $redirect)
    {
        $config['upload_path'] = 'public/upload/product';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = 5000;
        $config['max_width'] = 850;
        $config['max_height'] = 1036;
        $config['file_name'] = $filename;
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if (!$this->upload->do_upload('product_default_image')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('error', $error);
            redirect($redirect);
        } else {
            $fdata = $this->upload->data();
            $resize_conf = array(
                'source_image' => $fdata['full_path'],
                'new_image' => $fdata['file_path'] . '/thumbnail',
                'width' => 420,
                'height' => 512,
                'maintain_ratio' => true,
                'overwrite' => true
            );

            $this->image_lib->initialize($resize_conf);
            $this->image_lib->resize();
            return $fdata['file_name'];
        }
    }

    public function updateProductImage($filename, $redirect, $product_id)
    {
        $config['upload_path'] = 'public/upload/product';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = 5000;
        $config['max_width'] = 850;
        $config['max_height'] = 1036;
        $config['file_name'] = $filename;
        $this->load->library('upload', $config);
        $this->load->library('image_lib');


        if (!$this->upload->do_upload('product_default_image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('', '')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($redirect);
            }
        } else {

            $fdata = $this->upload->data();
            if ($fdata['file_name'] == '') {
                return;
            } else {

                $model = new Common_model();
                $path = $model->selectRow('tbl_product', $product_id, 'product_id');
                if ($path->product_default_image) {
                    unlink('public/upload/product/'.$path->product_default_image);
                    unlink('public/upload/product/thumbnail/'.$path->product_default_image);
                }

                $resize_conf = array(
                    'source_image' => $fdata['full_path'],
                    'new_image' => $fdata['file_path'] . '/thumbnail',
                    'width' => 420,
                    'height' => 512,
                    'maintain_ratio' => true,
                    'overwrite' => true
                );

                $this->image_lib->initialize($resize_conf);
                $this->image_lib->resize();
                return $fdata['file_name'];
            }
        }
    }


    public function selectAllProductByCategories($m_id,$c_id,$s_id){
       return $this->db->join('tbl_main_category','tbl_product.main_category_id=tbl_main_category.main_category_id')
           ->join('tbl_category','tbl_product.category_id=tbl_category.category_id')
           ->join('tbl_sub_category','tbl_product.sub_category_id=tbl_sub_category.sub_category_id')
           ->where('tbl_main_category.main_category_id',$m_id)
           ->where('tbl_category.category_id',$c_id)
           ->where('tbl_sub_category.sub_category_id',$s_id)
           ->order_by('tbl_product.created_at','desc')
           ->get('tbl_product')
           ->result();
    }

    public function selectProductByProductId($id){
        return $this->db->where('product_id',$id)
            ->join('tbl_main_category','tbl_product.main_category_id=tbl_main_category.main_category_id')
            ->join('tbl_category','tbl_product.category_id=tbl_category.category_id')
            ->join('tbl_sub_category','tbl_product.sub_category_id=tbl_sub_category.sub_category_id')
            ->get('tbl_product')
            ->row();
    }

    //For home featured product use only
    public function selectProductByCategoryId($id){
        return $this->db->where('category_id',$id)
            ->where('product_status',1)
            ->order_by('created_at','desc')
            ->limit(6)
            ->get('tbl_product')
            ->result();
    }

    /*
     * SELECT All Product Using Subcategory ID
     * Using For Site->All Category Page
     */
    public function selectAllActiveProductBySubcategoryId($id,$per_page,$offset){
        if($offset==null)
        {
            $offset=0;
        }
        return $this->db->where('product_status',1)
            ->where('sub_category_id',$id)
            ->order_by('created_at','desc')
            ->limit($per_page,$offset)
            ->get('tbl_product')
            ->result();
    }

    /*
     * SELECT All Product Using Category ID
     * Using For Site->All Category Page
     */
    public function selectAllActiveProductByCategoryId($id,$per_page,$offset){
        if($offset==null)
        {
            $offset=0;
        }
        return $this->db->where('product_status',1)
            ->where('category_id',$id)
            ->order_by('created_at','desc')
            ->limit($per_page,$offset)
            ->get('tbl_product')
            ->result();
    }

    /*
     * Select Sub Category Id By product id
     * Using For Site->product details
     */

    public function selectSubCategoryIdByProductId($product_id){
        return $this->db->select('sub_category_id')
            ->where('product_id',$product_id)
            ->get('tbl_product')
            ->row();
    }

    /*
     * Select All Product by Sub Category Id
     * Using For Home->product details -> Related Product Part
     */

    public function selectAllProductBySubCategoryId($id,$product_id){
        return $this->db->where('product_status',1)
            ->where('sub_category_id',$id)
            ->where_not_in('product_id',$product_id)
            ->order_by('created_at','desc')
            ->limit(8)
            ->get('tbl_product')
            ->result();
    }

}