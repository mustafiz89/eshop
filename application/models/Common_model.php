<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Common_model extends CI_Model{

    public function create($table,$data){
        return $this->db->insert($table,$data);
    }

    public function insert_batch($table,$data){
        return $this->db->insert_batch($table,$data);
    }
    public function update($table,$data,$id,$column){
        return $this->db->where($column,$id)->update($table,$data);
    }

    public function delete($table,$id,$column){
        return $this->db->where($column,$id)->delete($table);
    }

    public function selectAllRow($table){
        return $this->db->get($table)->result();

    }

    public function selectRow($table,$id,$column){
        return $this->db->where($column,$id)->get($table)->row();
    }

    public function storeImage($folder,$redirect){ //Image must be required
        $config['upload_path']          = 'public/admin/images/'.$folder.'/';
        $config['allowed_types']        = 'jpeg|jpg|png|gif';
        $config['max_size']             = 2000;
        $config['max_width']            = 2000;
        $config['max_height']           = 1500;
        $error='';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('message',$error);
            redirect($redirect);
        } else {
            $fdata = $this->upload->data();
            return $config['upload_path'] . $fdata['file_name'];
        }
    }

    public function saveImage($folder,$redirect){// Image Not required
        $config['upload_path']          = 'public/admin/images/'.$folder.'/';
        $config['allowed_types']        = 'jpeg|jpg|png|gif';
        $config['max_size']             = 2000;
        $config['max_width']            = 2000;
        $config['max_height']           = 1500;


        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                $error =$this->upload->display_errors();
                $this->session->set_userdata('error', $error);
                redirect($redirect);
            }
        } else {
            $fdata = $this->upload->data();
            return $config['upload_path'] . $fdata['file_name'];
        }
    }

    public function updateImage($folder,$redirect,$table,$id,$column){
        $config['upload_path']          = 'public/admin/images/'.$folder.'/';
        $config['allowed_types']        = 'jpeg|jpg|png|gif';
        $config['max_size']             = 2000;
        $config['max_width']            = 2000;
        $config['max_height']           = 1500;
        $error='';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('founder_image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('error', $error);
                redirect($redirect);
            }
        } else {
            $fdata = $this->upload->data();

            if ($fdata['file_name'] == '') {

                return;
            }
            else{
                $path=$this->selectRow($table,$id,$column);
                if($path->image)
                {
                    $image=$path->image;
                    unlink($image);
                }
                return $config['upload_path'] . $fdata['file_name'];
            }
        }
    }


    public function selectCustomerWithDistrict($customer_id){

        return $this->db->where('customer_id',$customer_id)
            ->join('tbl_district','tbl_district.district_id=tbl_customer.district_id')
            ->get('tbl_customer')->row();

    }

    public function get_mysql_last_id($table,$column){
        return $this->db->limit(1)
            ->order_by($column,'desc')
            ->get($table)
            ->row();
    }

//    Check credential for customer login

    public function check_customer_credentials($params){
        return $this->db->select('*')
            ->from('tbl_customer')
            ->where('email', $params['email'])
            ->where('password', $params['password'])
            ->get()->row();
    }

    public function order_list_by_date($customer_id){
        return $this->db->where('customer_id',$customer_id)
            ->order_by('created_at','desc')
            ->get('tbl_order')
            ->result();
    }


    public function get_order_list_by_order_id($order_table_id){
        return $this->db->where('order_table_id',$order_table_id)
            ->get('tbl_order_details')
            ->result();
    }

    public function total_price($order_table_id){
        return $this->db->select_sum('subtotal')
            ->where('order_table_id',$order_table_id)
            ->get('tbl_order_details')
            ->row();
    }

    public function get_order_by_order_id($order_table_id){
        return $this->db->select('tbl_order.*,c.l_name,c.f_name,c.gender,c.email,c.address_1,c.address_2,c.mobile_phone,c.home_phone,d.district_name')
            ->where('order_table_id',$order_table_id)
            ->join('tbl_customer as c','tbl_order.customer_id=c.customer_id')
            ->join('tbl_district as d','c.district_id=d.district_id')
            ->get('tbl_order')
            ->row();
    }
    // Php mailer

    public function sendMail($subject,$receiver,$body){
        $this->load->library("phpmailer");
        $mailer = new PHPMailer(true);

        $mailer->IsHTML(true);
        $mailer->IsSMTP();
        $mailer->SMTPDebug = 1;
        $mailer->SMTPAuth = true;
        $mailer->SMTPDebug  = 1;
        $mailer->Host = "mail.mustafiz.info";
        $mailer->Port = 26;
        $mailer->Username = 'eshop@mustafiz.info';
        $mailer->Password = 'eshop24';

        $mailer->AddAddress($receiver);
//        $mailer->addCC('name.cse815@gmail.com');

        $mailer->AddReplyTo("eshop@mustafiz.info", "Eshop");
        $mailer->From = "eshop@mustafiz.info";
        $mailer->FromName = "Eshop";

        $mailer->Subject = $subject;
        $mailer->Body = $body;

        try
        {
            $rr = $mailer->Send();
            return array(
                'msg' => 'send',
                'success' => TRUE
            );
        }
        catch (Exception $ex)
        {
            return array(
                'msg' => $ex,
                'success' => FALSE
            );
        }
    }


}
