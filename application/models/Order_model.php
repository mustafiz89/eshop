<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Order_model extends CI_Model
{
    public function get_pending_order(){
        return $this->db->select('tbl_order.*,c.l_name,c.f_name,c.gender,c.email,c.address_1,c.address_2,c.mobile_phone,c.home_phone,d.district_name')
            ->where('confirmed_status',0)
            ->join('tbl_customer as c','tbl_order.customer_id=c.customer_id')
            ->join('tbl_district as d','c.district_id=d.district_id')
            ->order_by('tbl_order.created_at','DESC')
            ->get('tbl_order')
            ->result();
    }
    public function get_complete_order(){
        return $this->db->select('tbl_order.*,c.l_name,c.f_name,c.gender,c.email,c.address_1,c.address_2,c.mobile_phone,c.home_phone,d.district_name')
            ->where('confirmed_status',1)
            ->join('tbl_customer as c','tbl_order.customer_id=c.customer_id')
            ->join('tbl_district as d','c.district_id=d.district_id')
            ->order_by('tbl_order.confirmed_at','DESC')
            ->get('tbl_order')
            ->result();
    }

}