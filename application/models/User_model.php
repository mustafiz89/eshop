<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class User_model extends CI_Model
{

    public function checkCredential($params)
    {
        return $this->db->select('*')
            ->from('tbl_user')
            ->where('username', $params['username'])
            ->where('password', $params['password'])
            ->get()->row();
    }

    public function saveUser($params){
        $this->db->insert('tbl_user',$params);
    }
    public function checkEmail($email)
    {
        return $this->db->where('email', $email)->get('tbl_user')->row();
    }

    public function getUser($id, $column)
    {
        return $this->db->where($column, $id)->get('tbl_user')->row();
    }

    public function getAllUser()
    {
        return $this->db->get('tbl_user')->result();
    }

    public function updateUser($id,$params)
    {
        $this->db->where('user_id', $id)->update('tbl_user', $params);
    }

    public function deleteUser($id){
        $this->db->where('user_id',$id)->delete('tbl_user');
    }

    public function saveUserImage($filename,$redirect){
        $config['upload_path'] = 'public/admin/images/' . $filename . '/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = 2000;
        $config['max_width'] = 2000;
        $config['max_height'] = 1500;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('error', $error);
                redirect($redirect);

        } else {
            $fdata = $this->upload->data();
            return $config['upload_path'] . $fdata['file_name'];

        }
    }

    public function updateUserImage($filename,$id)
    {
        $config['upload_path'] = 'public/admin/images/' . $filename . '/';
        $config['allowed_types'] = 'jpeg|jpg|png|gif';
        $config['max_size'] = 2000;
        $config['max_width'] = 2000;
        $config['max_height'] = 1500;
        $error = '';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            if ("You did not select a file to upload." != $this->upload->display_errors('', '')) {
                $error = $this->upload->display_errors();
                $this->session->set_userdata('error', $error);
                redirect('user');
            }
        } else {
            $fdata = $this->upload->data();
            if ($fdata['file_name'] == '') {
                return;
            } else {
                $path = $this->getUser($id,'user_id');
                if ($path->image) {
                    $image = $path->image;
                    unlink($image);
                }
                return $config['upload_path'] . $fdata['file_name'];
            }
        }
    }
}