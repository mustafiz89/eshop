<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Category_model extends CI_Model{

    public function selectAllActiveMainCategory()
    {
        return $this->db->where('main_category_status', 1)
            ->get('tbl_main_category')
            ->result();
    }

    public function selectAllCategory(){
       return $this->db->join('tbl_main_category','tbl_category.main_category_id=tbl_main_category.main_category_id')
            ->get('tbl_category')
            ->result();
    }

    public function selectCategoryById($id){
       return $this->db->where('category_id',$id)
            ->get('tbl_category')
            ->row();
    }



    public function selectAllActiveCategory(){
        return $this->db->where('category_status',1)
            ->get('tbl_category')
            ->result();
    }

    public function selectCategoryByMainCategoryId($id){//Ajax call model function
        return $this->db->where('main_category_id',$id)
                    ->where('category_status',1)
                    ->get('tbl_category')
                    ->result();

    }

    public function selectAllSubcategoryByIds($main_category_id,$category_id){
        return $this->db->join('tbl_main_category','tbl_sub_category.main_category_id=tbl_main_category.main_category_id')
            ->join('tbl_category','tbl_sub_category.category_id=tbl_category.category_id')
            ->where('tbl_sub_category.main_category_id',$main_category_id)
            ->where('tbl_sub_category.category_id',$category_id)
            ->get('tbl_sub_category')
            ->result();
    }

    public function selectSubcategoryByIds($main_category_id,$category_id){
        return $this->db->join('tbl_main_category','tbl_sub_category.main_category_id=tbl_main_category.main_category_id')
            ->join('tbl_category','tbl_sub_category.category_id=tbl_category.category_id')
            ->where('tbl_sub_category.main_category_id',$main_category_id)
            ->where('tbl_sub_category.category_id',$category_id)
            ->get('tbl_sub_category')
            ->row();
    }

    public function selectSubCategoryById($sub_category_id){
        return  $this->db->where('sub_category_id',$sub_category_id)
            ->get('tbl_sub_category')
            ->row();
    }

    public function selectAllCategoryImageById($category_id){
        return $this->db->where('category_id',$category_id)
            ->get('tbl_category_image')
            ->result();
    }

    public function selectSubCategoryByCategoryId($category_id){
        return $this->db->where('category_id',$category_id)
            ->where('sub_category_status',1)
            ->get('tbl_sub_category')
            ->result();
    }

    public function selectCategoryIdBySubCategoryId($sub_category_id){
       return $this->db->select('category_id')
            ->where('sub_category_id',$sub_category_id)
            ->group_by('category_id')
            ->get('tbl_sub_category')
            ->row();

    }

    /*
     * Home page Model Begin
     */
    //    For Home page use
    public function selectAllActiveCategoryByWidget(){
        return $this->db->where('category_status',1)
            ->where('widget_status',1)
            ->get('tbl_category')
            ->result();
    }

    //For Home page use
    public function selectAllActiveCategoryByMainCategoryId($main_category_id){
        return $this->db->where('category_status',1)
            ->where('main_category_id',$main_category_id)
            ->get('tbl_category')
            ->result();
    }
    //For Home page use
    public function selectAllActiveSubCategoryByCategoryId($category_id){
        return  $this->db->where('category_id',$category_id)
            ->where('sub_category_status',1)
            ->get('tbl_sub_category')
            ->result();
    }
    /*
    * Home page Model End
    */
}