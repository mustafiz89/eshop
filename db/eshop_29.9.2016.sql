-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2016 at 08:47 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_status` tinyint(1) DEFAULT NULL,
  `widget_status` tinyint(1) NOT NULL DEFAULT '0',
  `widget_title` varchar(255) NOT NULL,
  `widget_color` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `main_category_id`, `category_name`, `category_status`, `widget_status`, `widget_title`, `widget_color`, `created_at`, `updated_at`) VALUES
(3, 4, 'প্যান্ট', 1, 1, 'ছেলেদের প্যান্ট', '#6e38e3', NULL, '2016-09-28 10:37:46'),
(4, 4, 'শার্ট', 1, 1, 'শার্ট', '#c33d19', '2016-09-28 22:23:20', '2016-09-28 16:46:02'),
(5, 5, 'শাড়ী', 1, 1, 'শাড়ী', '#ee6ff4', '2016-09-28 22:49:02', '2016-09-28 16:52:36'),
(6, 5, 'সালোয়ার কামিজ', 1, 1, 'সালোয়ার কামিজ', '#e4045a', '2016-09-28 22:55:42', '2016-09-28 16:56:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_image`
--

CREATE TABLE `tbl_category_image` (
  `category_image_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `widget_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category_image`
--

INSERT INTO `tbl_category_image` (`category_image_id`, `category_id`, `widget_image`, `created_at`) VALUES
(45, 3, 'public/upload/category/16-09-28_1475069780_0_3.jpg', '2016-09-28 13:36:20'),
(46, 3, 'public/upload/category/16-09-28_1475071006_0_3.jpg', '2016-09-28 13:56:46'),
(47, 3, 'public/upload/category/16-09-28_1475071152_0_3.jpg', '2016-09-28 13:59:12'),
(48, 4, 'public/upload/category/16-09-28_1475081168_0_4.jpg', '2016-09-28 16:46:08'),
(49, 4, 'public/upload/category/16-09-28_1475081252_0_4.jpg', '2016-09-28 16:47:32'),
(50, 5, 'public/upload/category/16-09-28_1475081506_0_5.jpg', '2016-09-28 16:51:46'),
(51, 5, 'public/upload/category/16-09-28_1475081506_0_51.jpg', '2016-09-28 16:51:46'),
(52, 6, 'public/upload/category/16-09-28_1475081891_0_6.jpg', '2016-09-28 16:58:11'),
(53, 6, 'public/upload/category/16-09-28_1475081891_0_61.jpg', '2016-09-28 16:58:11'),
(54, 5, 'public/upload/category/16-09-28_1475084453_0_5.jpg', '2016-09-28 17:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_category`
--

CREATE TABLE `tbl_main_category` (
  `main_category_id` int(11) NOT NULL,
  `main_category_name` varchar(255) DEFAULT NULL,
  `main_category_status` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_main_category`
--

INSERT INTO `tbl_main_category` (`main_category_id`, `main_category_name`, `main_category_status`, `created_at`, `updated_at`) VALUES
(4, 'ছেলেদের শপিং', 1, '2016-09-20 23:15:19', '2016-09-20 17:23:23'),
(5, 'মেয়েদের শপিং', 1, '2016-09-28 21:03:05', '2016-09-28 15:03:05'),
(7, 'বেবী অ্যান্ড কিডস', 1, '2016-09-28 21:05:05', '2016-09-28 15:05:05'),
(8, 'কসমেটিক্স ও পারফিউম', 1, '2016-09-28 21:05:55', '2016-09-28 15:10:19'),
(9, 'গহনা ও ঘড়ি', 1, '2016-09-28 21:06:26', '2016-09-28 15:11:08'),
(10, 'গৃহসজ্জা', 1, '2016-09-28 21:07:56', '2016-09-28 15:07:56'),
(11, 'গৃহস্থালী সামগ্রী', 1, '2016-09-28 21:08:18', '2016-09-28 15:08:18'),
(12, 'শৌখিন ও উপহার সামগ্রী', 1, '2016-09-28 21:09:00', '2016-09-28 15:09:00'),
(13, 'গ্যাজেটস ও ইলেকট্রনিক্স', 1, '2016-09-28 21:12:53', '2016-09-28 15:12:53'),
(14, 'মোবাইল ও ট্যাব', 1, '2016-09-28 21:13:18', '2016-09-28 15:13:18'),
(15, 'কম্পিউটার এক্সেসরিজ', 1, '2016-09-28 21:18:08', '2016-09-28 15:18:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `product_price` varchar(255) DEFAULT '0',
  `discount` varchar(100) DEFAULT '0',
  `product_previous_price` varchar(255) DEFAULT NULL,
  `stock_status` tinyint(1) DEFAULT '0',
  `product_condition` tinyint(1) DEFAULT '0',
  `product_default_image` varchar(255) DEFAULT NULL,
  `product_details` text,
  `product_status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `main_category_id`, `category_id`, `sub_category_id`, `product_title`, `product_price`, `discount`, `product_previous_price`, `stock_status`, `product_condition`, `product_default_image`, `product_details`, `product_status`, `created_at`, `updated_at`) VALUES
(5, 4, 3, 2, 'সেমি ন্যারো ফিট মেনজ জিন্স প্যান্ট', '870', '0', NULL, 1, 1, '4_3_2_2016-09-27_1474948080.jpg', 'রেগুলার ফিট স্ক্র্যাচড জিন্স প্যান্ট<br><br>', 1, '2016-09-27 09:48:00', NULL),
(6, 4, 3, 2, 'রেগুলার ফিট স্ক্র্যাচড জিন্স প্যান্ট (কপি)', '970', '0', '1036', 1, 1, '4_3_2_2016-09-28_1475043521.jpg', 'রেগুলার ফিট স্ক্র্যাচড জিন্স প্যান্ট (কপি)<br><br>', 1, '2016-09-27 09:48:16', NULL),
(7, 4, 3, 3, 'স্লিম বুট কাট জিন্স', '1035', '0', NULL, 1, 1, '4_3_3_2016-09-28_1475063295.jpg', '', 1, '2016-09-28 17:48:15', NULL),
(8, 4, 3, 3, 'স্ট্রেইট ফিট জিন্স', '750', '0', NULL, 1, 1, '4_3_3_2016-09-28_1475063512.jpg', 'স্ট্রেইট ফিট জিন্স<br><br>', 1, '2016-09-28 17:51:52', NULL),
(9, 4, 3, 3, 'কেনটন জিন্স', '1370', '0', NULL, 1, 1, '4_3_3_2016-09-28_1475070348.jpg', 'কেনটন জিন্স<br><br>', 1, '2016-09-28 19:45:48', NULL),
(10, 4, 3, 2, 'স্ক্র্যাচড ডেনিম জিন্স', '2000', '0', NULL, 1, 1, '4_3_2_2016-09-28_1475070728.jpg', 'স্ক্র্যাচড ডেনিম জিন্স<br><br>', 1, '2016-09-28 19:52:08', NULL),
(11, 4, 4, 4, 'মেনজ ফরমাল শার্ট', '800', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083147.jpg', 'মেনজ ফরমাল শার্ট<br><br>', 1, '2016-09-28 23:19:07', NULL),
(12, 4, 4, 4, 'মেনজ ফরমাল সাদা শার্ট', '700', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083284.jpg', 'মেনজ ফরমাল সাদা শার্ট<br><br>', 1, '2016-09-28 23:21:24', NULL),
(13, 4, 4, 4, 'মেনজ ফরমাল নীল শার্ট', '840', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083370.jpg', 'মেনজ ফরমাল নীল শার্ট<br><br>', 1, '2016-09-28 23:22:50', NULL),
(14, 4, 4, 4, 'মেনজ ফরমাল কটন নীল শার্ট', '1020', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083494.jpg', 'মেনজ ফরমাল কটন নীল শার্ট<br><br>', 1, '2016-09-28 23:24:54', NULL),
(15, 4, 4, 4, 'মেনজ ফরমাল ডারবি শার্ট', '1200', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083583.jpg', 'মেনজ ফরমাল ডারবি শার্ট<br><br>', 1, '2016-09-28 23:26:23', NULL),
(16, 4, 4, 4, 'ফরমাল মেনজ কটন সাদা শার্ট', '850', '0', NULL, 1, 1, '4_4_4_2016-09-28_1475083659.jpg', 'ফরমাল মেনজ কটন সাদা শার্ট<br><br>', 1, '2016-09-28 23:27:39', NULL),
(17, 5, 5, 8, 'ইসিন কটন শাড়ী', '1500', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084561.jpg', 'ইসিন কটন শাড়ী<br><br>', 1, '2016-09-28 23:42:41', NULL),
(18, 5, 5, 8, 'পিউর কটন শাড়ী', '1650', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084632.jpg', 'পিউর কটন শাড়ী<br><br>', 1, '2016-09-28 23:43:52', NULL),
(19, 5, 5, 8, 'পিউর রেড কটন শাড়ী', '1850', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084748.jpg', 'পিউর রেড কটন শাড়ী<br><br>', 1, '2016-09-28 23:45:48', NULL),
(20, 5, 5, 8, 'পিউর মাল্টি কালার কটন শাড়ী', '1480', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084810.jpg', 'পিউর মাল্টি কালার কটন শাড়ী<br><br>', 1, '2016-09-28 23:46:50', NULL),
(21, 5, 5, 8, 'পিউর কটন তাতের শাড়ী', '1790', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084875.jpg', 'পিউর কটন তাতের শাড়ী<br><br>', 1, '2016-09-28 23:47:55', NULL),
(22, 5, 5, 8, 'গর্জিয়াস নীল পিউর কটন শাড়ী', '2100', '0', NULL, 1, 1, '5_5_8_2016-09-28_1475084951.jpg', 'গর্জিয়াস নীল পিউর কটন শাড়ী<br><br>', 1, '2016-09-28 23:49:11', NULL),
(23, 5, 6, 12, 'কটন থ্রি পিস', '750', '0', NULL, 1, 1, '5_6_12_2016-09-29_1475086346.jpg', 'কটন থ্রি পিস<br><br>', 1, '2016-09-29 00:12:26', NULL),
(24, 5, 6, 12, 'ইন্ডিয়ান কটন গ্রীন থ্রি পিস', '950', '0', '', 1, 1, '5_6_12_2016-09-29_1475086407.jpeg', 'ইন্ডিয়ান কটন গ্রীন থ্রি পিস<br><br>', 1, '2016-09-29 00:13:27', NULL),
(25, 5, 6, 12, 'থ্রি পিস নীল ও সবুজ মিক্সড কালার', '650', '0', NULL, 1, 1, '5_6_12_2016-09-29_1475086492.jpg', 'থ্রি পিস নীল ও সবুজ মিক্সড কালার<br><br>', 1, '2016-09-29 00:14:52', NULL),
(26, 5, 6, 12, 'কটন গ্রিন স্ট্রেইট থ্রি পিস', '780', '0', NULL, 1, 1, '5_6_12_2016-09-29_1475086646.jpg', 'কটন গ্রিন স্ট্রেইট থ্রি পিস<br><br>', 1, '2016-09-29 00:17:26', NULL),
(27, 5, 6, 12, 'কটন মিক্সড কালার থি পিছ', '1050', '0', NULL, 1, 1, '5_6_12_2016-09-29_1475086718.jpg', 'কটন মিক্সড কালার থি পিছ<br><br>', 1, '2016-09-29 00:18:38', NULL),
(28, 5, 6, 12, 'ইন্ডিয়ান কটন মিক্সড কালার থ্রি পিস', '1350', '0', NULL, 1, 1, '5_6_12_2016-09-29_1475086753.jpg', 'ইন্ডিয়ান কটন মিক্সড কালার থ্রি পিস<br><br>', 1, '2016-09-29 00:19:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_category`
--

CREATE TABLE `tbl_sub_category` (
  `sub_category_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `sub_category_name` varchar(255) DEFAULT NULL,
  `sub_category_status` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sub_category`
--

INSERT INTO `tbl_sub_category` (`sub_category_id`, `category_id`, `main_category_id`, `sub_category_name`, `sub_category_status`, `created_at`, `updated_at`) VALUES
(2, 3, 4, 'স্ক্র্যাচড জিন্স', 1, '2016-09-20 23:12:54', '2016-09-20 17:42:21'),
(3, 3, 4, 'জিন্স', 1, '2016-09-28 16:35:02', '2016-09-28 10:35:02'),
(4, 4, 4, 'ফর্মাল', 1, '2016-09-28 22:40:17', '2016-09-28 16:40:17'),
(5, 4, 4, 'ক্যাজুয়াল', 1, '2016-09-28 22:41:14', '2016-09-28 16:41:14'),
(6, 4, 4, 'ফুল-স্লিভ', 1, '2016-09-28 22:41:27', '2016-09-28 16:41:27'),
(7, 4, 4, 'হাফ-স্লিভ', 1, '2016-09-28 22:41:38', '2016-09-28 16:41:38'),
(8, 5, 5, 'সুতি', 1, '2016-09-28 23:00:03', '2016-09-28 17:00:03'),
(9, 5, 5, 'সিল্ক', 1, '2016-09-28 23:00:25', '2016-09-28 17:00:25'),
(10, 5, 5, 'কাতান', 1, '2016-09-28 23:00:36', '2016-09-28 17:00:36'),
(11, 5, 5, 'জামদানি', 1, '2016-09-28 23:00:51', '2016-09-28 17:00:51'),
(12, 6, 5, 'সুতি', 1, '2016-09-28 23:02:38', '2016-09-28 17:05:13'),
(13, 6, 5, 'দেশি বুটিক', 1, '2016-09-28 23:03:04', '2016-09-28 17:03:04'),
(14, 6, 5, 'লন', 1, '2016-09-28 23:03:18', '2016-09-28 17:03:18'),
(15, 6, 5, 'জর্জেট', 1, '2016-09-28 23:03:47', '2016-09-28 17:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `role` tinyint(4) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `create_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `full_name`, `username`, `password`, `email`, `role`, `image`, `create_at`, `update_at`) VALUES
(1, 'Admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'sohag2882@yahoo.com', 1, 'public/admin/images/user/1.jpg', '2016-07-27', '2016-07-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_category_image`
--
ALTER TABLE `tbl_category_image`
  ADD PRIMARY KEY (`category_image_id`);

--
-- Indexes for table `tbl_main_category`
--
ALTER TABLE `tbl_main_category`
  ADD PRIMARY KEY (`main_category_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_category_image`
--
ALTER TABLE `tbl_category_image`
  MODIFY `category_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tbl_main_category`
--
ALTER TABLE `tbl_main_category`
  MODIFY `main_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  MODIFY `sub_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
